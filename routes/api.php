<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([ 'prefix' => 'v1'], function () {

	Route::resource('register', 'v1\RegisterController');
	Route::resource('paintype', 'v1\PainTypeController');
	Route::resource('painlevel', 'v1\PainLevelController');
	Route::resource('painkiller', 'v1\PainKillersController');
	Route::resource('profession', 'v1\ProfessionsController');
	Route::resource('qualification', 'v1\QualificationsController');
	Route::resource('specialities', 'v1\SpecialitiesController');
	Route::resource('problems', 'v1\ProblemsController');
	Route::resource('user', 'v1\UserController');
	Route::post('register', 'v1\RegisterController@store');
	Route::post('registerProfessional', 'v1\RegisterController@professional');
	Route::post('login', 'v1\AuthController@login');
	Route::post('forgot', 'v1\AuthController@sendPasswordEmail');
	Route::post('reset-password', 'v1\ResetPasswordController@reset');
	Route::post('reviews', 'v1\ReviewController@store');
	Route::get('patientsMap/{location}', 'v1\PatientsController@patientsMap');
	Route::get('professionalsMap/{location}', 'v1\ProfessionalsController@professionalsMap');
	Route::get('getUserByUsername/{username}/{order}', 'v1\UserController@getUserByUsername');
	Route::get('getUserByEmail/{email}', 'v1\UserController@getUserByEmail');
	Route::get('preference', 'v1\PreferenceController@index');
	Route::get('country/{text}', 'v1\LocationController@getCountries');
	Route::get('region/{id}', 'v1\LocationController@getRegions');
	Route::get('city/{id}', 'v1\LocationController@getCities');
	Route::get('reviews/{username}', 'v1\ReviewController@show');
	Route::get('messagesBySubject/{subject}', 'v1\MessagesController@getBySubject');
	Route::get('messages', 'v1\MessagesController@index');
	Route::get('messages-deleted', 'v1\MessagesController@deleted');
	Route::get('messages-sent', 'v1\MessagesController@sent');
	Route::get('message/{id}', 'v1\MessagesController@show');
	Route::get('composeMessage/{username}', 'v1\MessagesController@compose');
	Route::post('postMessage', 'v1\MessagesController@store');
	Route::put('updatePassword', 'v1\UserController@updatePassword');
	Route::put('updatePerference', 'v1\UserController@updatePerference');
	Route::put('updateMessage', 'v1\MessagesController@update');
	Route::put('deleteMessage', 'v1\MessagesController@delete');
	Route::get('professionals', 'v1\ProfessionalsController@index');
	Route::get('professionals/{long}/{lat}', 'v1\ProfessionalsController@indexMap');
	Route::get('professionals/{username}', 'v1\ProfessionalsController@show');
	Route::get('professionalsFilters/{pros}/{spec}/{location}/{order}/{disMin}/{disMax}/{long}/{lat}', 'v1\ProfessionalsController@filter');	
	Route::get('patients', 'v1\PatientsController@index');
	Route::get('patients/{long}/{lat}', 'v1\PatientsController@indexMap');
	Route::get('patients/{username}', 'v1\PatientsController@show');
	Route::get('patientsFilters/{painType}/{location}/{order}/{disMin}/{disMax}/{painMin}/{painMax}/{painKillers}/{long}/{lat}', 'v1\PatientsController@filter');
	/*Route::get('/', ['as' => 'messages', 'uses' => 'v1\MessagesController@index']);
	Route::get('create', ['as' => 'messages.create', 'uses' => 'v1\MessagesController@create']);
	Route::post('/', ['as' => 'messages.store', 'uses' => 'v1\MessagesController@store']);
	Route::get('{id}', ['as' => 'messages.show', 'uses' => 'v1\MessagesController@show']);
	Route::put('{id}', ['as' => 'messages.update', 'uses' => 'v1\MessagesController@update']);*/
	
	Route::group(['middleware' => ['before' => 'jwt.auth']], function () {
		Route::resource('post-job', 'v1\PostJobController');
		Route::post('reviews', 'v1\ReviewController@store');
		Route::get('profile', 'v1\AuthController@profile');
		Route::get('jobs/listjob', 'v1\PostJobController@getMyJobs');
	});

});
