<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/admin', 'HomeController@index')->name('home');

Route::get('/admin/patients', 'v1\PatientsController@admin');
Route::get('/admin/patients-add', 'v1\PatientsController@create');
Route::post('/admin/patients-add', 'v1\PatientsController@add');
Route::get('/admin/patients-edit/{id}', 'v1\PatientsController@edit');
Route::post('/admin/patients-edit/{id}', 'v1\PatientsController@update');
Route::get('/admin/patients-delete/{id}', 'v1\PatientsController@user');
Route::post('/admin/patients-delete/{id}', 'v1\PatientsController@destroy');

Route::get('/admin/painkillers', 'v1\PainKillersController@admin');
Route::get('/admin/painkillers-add', 'v1\PainKillersController@create');
Route::post('/admin/painkillers-add', 'v1\PainKillersController@store');
Route::get('/admin/painkillers-edit/{id}', 'v1\PainKillersController@edit');
Route::post('/admin/painkillers-edit/{id}', 'v1\PainKillersController@update');
Route::get('/admin/painkillers-delete/{id}', 'v1\PainKillersController@show');
Route::post('/admin/painkillers-delete/{id}', 'v1\PainKillersController@destroy');

Route::get('/admin/painlevel', 'v1\PainLevelController@admin');
Route::get('/admin/painlevel-add', 'v1\PainLevelController@create');
Route::post('/admin/painlevel-add', 'v1\PainLevelController@store');
Route::get('/admin/painlevel-edit/{id}', 'v1\PainLevelController@edit');
Route::post('/admin/painlevel-edit/{id}', 'v1\PainLevelController@update');
Route::get('/admin/painlevel-delete/{id}', 'v1\PainLevelController@show');
Route::post('/admin/painlevel-delete/{id}', 'v1\PainLevelController@destroy');

Route::get('/admin/paintype', 'v1\PainTypeController@admin');
Route::get('/admin/paintype-add', 'v1\PainTypeController@create');
Route::post('/admin/paintype-add', 'v1\PainTypeController@store');
Route::get('/admin/paintype-edit/{id}', 'v1\PainTypeController@edit');
Route::post('/admin/paintype-edit/{id}', 'v1\PainTypeController@update');
Route::get('/admin/paintype-delete/{id}', 'v1\PainTypeController@show');
Route::post('/admin/paintype-delete/{id}', 'v1\PainTypeController@destroy');

Route::get('/admin/professionals', 'v1\ProfessionalsController@admin');
Route::get('/admin/professionals-add', 'v1\ProfessionalsController@create');
Route::post('/admin/professionals-add', 'v1\ProfessionalsController@add');
Route::get('/admin/professionals-edit/{id}', 'v1\ProfessionalsController@edit');
Route::post('/admin/professionals-edit/{id}', 'v1\ProfessionalsController@update');
Route::get('/admin/professionals-delete/{id}', 'v1\ProfessionalsController@user');
Route::post('/admin/professionals-delete/{id}', 'v1\ProfessionalsController@destroy');

Route::get('/admin/professions', 'v1\ProfessionsController@admin');
Route::get('/admin/professions-add', 'v1\ProfessionsController@create');
Route::post('/admin/professions-add', 'v1\ProfessionsController@store');
Route::get('/admin/professions-edit/{id}', 'v1\ProfessionsController@edit');
Route::post('/admin/professions-edit/{id}', 'v1\ProfessionsController@update');
Route::get('/admin/professions-delete/{id}', 'v1\ProfessionsController@show');
Route::post('/admin/professions-delete/{id}', 'v1\ProfessionsController@destroy');

Route::get('/admin/qualifications', 'v1\QualificationsController@admin');
Route::get('/admin/qualifications-add', 'v1\QualificationsController@create');
Route::post('/admin/qualifications-add', 'v1\QualificationsController@store');
Route::get('/admin/qualifications-edit/{id}', 'v1\QualificationsController@edit');
Route::post('/admin/qualifications-edit/{id}', 'v1\QualificationsController@update');
Route::get('/admin/qualifications-delete/{id}', 'v1\QualificationsController@show');
Route::post('/admin/qualifications-delete/{id}', 'v1\QualificationsController@destroy');

Route::get('/admin/specialities', 'v1\SpecialitiesController@admin');
Route::get('/admin/specialities-add', 'v1\SpecialitiesController@create');
Route::post('/admin/specialities-add', 'v1\SpecialitiesController@store');
Route::get('/admin/specialities-edit/{id}', 'v1\SpecialitiesController@edit');
Route::post('/admin/specialities-edit/{id}', 'v1\SpecialitiesController@update');
Route::get('/admin/specialities-delete/{id}', 'v1\SpecialitiesController@show');
Route::post('/admin/specialities-delete/{id}', 'v1\SpecialitiesController@destroy');
