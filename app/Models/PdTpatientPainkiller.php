<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 04 May 2018 06:31:35 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PdTpatientPainkiller
 * 
 * @property int $pkpatient_painkiller
 * @property int $fkpatient
 * @property int $fkpainkiller
 * 
 * @property \App\Models\PdTpatient $pd_tpatient
 * @property \App\Models\PdTpainkiller $pd_tpainkiller
 *
 * @package App\Models
 */
class PdTpatientPainkiller extends Eloquent
{
	protected $table = 'pd_tpatient_painkiller';
	protected $primaryKey = 'pkpatient_painkiller';
	public $timestamps = false;

	protected $casts = [
		'fkpatient' => 'int',
		'fkpainkiller' => 'int'
	];

	protected $fillable = [
		'fkpatient',
		'fkpainkiller'
	];

	public function pd_tpatient()
	{
		return $this->belongsTo(\App\Models\PdTpatient::class, 'fkpatient');
	}

	public function pd_tpainkiller()
	{
		return $this->belongsTo(\App\Models\PdTpainkiller::class, 'fkpainkiller');
	}
}
