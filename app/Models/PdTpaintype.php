<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 08:56:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PdTpaintype
 * 
 * @property int $pkpaintype
 * @property string $paintype_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $pd_tpatients
 *
 * @package App\Models
 */
class PdTpaintype extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'pd_tpaintype';
	protected $primaryKey = 'pkpaintype';

	protected $fillable = [
		'paintype_name'
	];

	protected $hidden = ['created_at, deleted_at, updated_at'];

	public function pd_tpatients()
	{
		return $this->hasMany(\App\Models\PdTpatient::class, 'fkpaintype');
	}
}
