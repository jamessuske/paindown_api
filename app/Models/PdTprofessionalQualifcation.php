<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 08:56:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PdTprofessionalQualifcation
 * 
 * @property int $fkprofessional
 * @property int $fkqualification
 * 
 * @property \App\Models\PdTprofessional $pd_tprofessional
 * @property \App\Models\PdTqualification $pd_tqualification
 *
 * @package App\Models
 */
class PdTprofessionalQualifcation extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'fkprofessional' => 'int',
		'fkqualification' => 'int'
	];

	public function pd_tprofessional()
	{
		return $this->belongsTo(\App\Models\PdTprofessional::class, 'fkprofessional');
	}

	public function pd_tqualification()
	{
		return $this->belongsTo(\App\Models\PdTqualification::class, 'fkqualification');
	}
}
