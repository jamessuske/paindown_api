<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 08:56:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PdTmembership
 * 
 * @property int $pkmembership
 * @property string $membership_name
 * @property string $membership_price
 * @property int $membership_duration
 * @property string $membership_duration_name
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $pd_tmembership_controls
 * @property \Illuminate\Database\Eloquent\Collection $pd_tpatients
 * @property \Illuminate\Database\Eloquent\Collection $pd_tprofessionals
 *
 * @package App\Models
 */
class PdTmembership extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'pd_tmembership';
	protected $primaryKey = 'pkmembership';

	protected $casts = [
		'membership_duration' => 'int'
	];

	protected $fillable = [
		'membership_name',
		'membership_price',
		'membership_duration',
		'membership_duration_name'
	];

	public function pd_tmembership_controls()
	{
		return $this->hasMany(\App\Models\PdTmembershipControl::class, 'fkmembership');
	}

	public function pd_tpatients()
	{
		return $this->hasMany(\App\Models\PdTpatient::class, 'fkmembership');
	}

	public function pd_tprofessionals()
	{
		return $this->hasMany(\App\Models\PdTprofessional::class, 'fkmembership');
	}
}
