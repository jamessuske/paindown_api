<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 08:56:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PdTprofessional
 * 
 * @property int $pkprofessional
 * @property string $professional_name
 * @property string $professional_username
 * @property string $professional_address
 * @property string $professional_phone
 * @property int $fkcity
 * @property int $fkmembership
 * @property \Carbon\Carbon $professional_membershipvalidity
 * @property string $professional_website
 * @property int $fkprofession
 * @property string $professional_about
 * @property string $professional_approach
 * @property string $professional_stories
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\PdTmembership $pd_tmembership
 * @property \App\Models\PdTprofession $pd_tprofession
 * @property \App\Models\PdTcity $pd_tcity
 * @property \Illuminate\Database\Eloquent\Collection $pd_tlogins
 * @property \Illuminate\Database\Eloquent\Collection $pd_tpatient_professional_chats
 * @property \Illuminate\Database\Eloquent\Collection $pd_tprofessional_preferences
 * @property \Illuminate\Database\Eloquent\Collection $pd_tprofessional_qualifcations
 * @property \Illuminate\Database\Eloquent\Collection $pd_tprofessional_specialities
 * @property \Illuminate\Database\Eloquent\Collection $pd_treviews
 *
 * @package App\Models
 */
class PdTprofessional extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'pd_tprofessional';
	protected $primaryKey = 'pkprofessional';

	protected $casts = [
		'fkcountry' => 'int',
		'fkregion' => 'int',
		'fkcity' => 'int',
		'fkmembership' => 'int',
		'fkprofession' => 'int'
	];

	protected $dates = [
		'professional_membershipvalidity'
	];

	protected $fillable = [
		'professional_name',
		'professional_username',
		'professional_address',
		'professional_phone',
		'professional_image',
		'fkcountry',
		'fkregion',
		'fkcity',
		'fkmembership',
		'professional_membershipvalidity',
		'professional_website',
		'fkprofession',
		'professional_about',
		'professional_approach',
		'professional_stories'
	];

	public function pd_tmembership()
	{
		return $this->belongsTo(\App\Models\PdTmembership::class, 'fkmembership');
	}

	public function pd_tprofession()
	{
		return $this->belongsTo(\App\Models\PdTprofession::class, 'fkprofession');
	}


	public function pd_tcountry()
        {
                return $this->belongsTo(\App\Models\PdTcountry::class, 'fkcountry');
        }
        public function pd_tregion()
        {
                return $this->belongsTo(\App\Models\PdTregion::class, 'fkregion');
        }

	public function pd_tcity()
	{
		return $this->belongsTo(\App\Models\PdTcity::class, 'fkcity');
	}

	public function pd_tlogins()
	{
		return $this->hasMany(\App\Models\PdTlogin::class, 'fkprofessional');
	}

	public function pd_tpatient_professional_chats()
	{
		return $this->hasMany(\App\Models\PdTpatientProfessionalChat::class, 'fkprofessional');
	}

	public function pd_tprofessional_preferences()
	{
		return $this->hasMany(\App\Models\PdTprofessionalPreference::class, 'fkprofessional');
	}

	public function pd_tprofessional_qualifcations()
	{
		return $this->hasMany(\App\Models\PdTprofessionalQualifcation::class, 'fkprofessional');
	}

	public function pd_tprofessional_specialities()
	{
		return $this->hasMany(\App\Models\PdTprofessionalSpeciality::class, 'fkprofessional');
	}

	public function pd_treviews()
	{
		return $this->hasMany(\App\Models\PdTreview::class, 'fkprofessional');
	}
}
