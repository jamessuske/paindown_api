<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 08:56:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PdTspeciality
 * 
 * @property int $pkspecialty
 * @property string $specialty_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $pd_tprofessional_specialities
 *
 * @package App\Models
 */
class PdTspeciality extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $primaryKey = 'pkspecialty';

	protected $fillable = [
		'specialty_name'
	];

	public function pd_tprofessional_specialities()
	{
		return $this->hasMany(\App\Models\PdTprofessionalSpeciality::class, 'fkspecialty');
	}
}
