<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 08:56:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PdTadmin
 * 
 * @property int $pkadmin
 * @property string $admin_name
 * 
 * @property \Illuminate\Database\Eloquent\Collection $pd_tlogins
 *
 * @package App\Models
 */
class PdTadmin extends Eloquent
{
	protected $table = 'pd_tadmin';
	protected $primaryKey = 'pkadmin';
	public $timestamps = false;

	protected $fillable = [
		'admin_name'
	];

	public function pd_tlogins()
	{
		return $this->hasMany(\App\Models\PdTlogin::class, 'fkadmin');
	}
}
