<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 08:56:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PdTqualification
 * 
 * @property int $pkqualification
 * @property string $qualification_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $pd_tprofessional_qualifcations
 *
 * @package App\Models
 */
class PdTqualification extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $primaryKey = 'pkqualification';

	protected $fillable = [
		'qualification_name'
	];

	public function pd_tprofessional_qualifcations()
	{
		return $this->hasMany(\App\Models\PdTprofessionalQualifcation::class, 'fkqualification');
	}
}
