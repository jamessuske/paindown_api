<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 08:56:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PdTpreference
 * 
 * @property int $pkpreference
 * @property string $preference_name
 * @property int $preference_type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $pd_tpatient_preferences
 * @property \Illuminate\Database\Eloquent\Collection $pd_tprofessional_preferences
 *
 * @package App\Models
 */
class PdTpreference extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'pd_tpreference';
	protected $primaryKey = 'pkpreference';

	protected $casts = [
		'pkpreference' => 'int',
		'preference_name' => 'varchar',
		'preference_type' => 'int'
	];

	protected $fillable = [
		'preference_name',
		'preference_type'
	];

	public function pd_tpatient_preferences()
	{
		return $this->hasMany(\App\Models\PdTpatientPreference::class, 'fkpreference');
	}

	public function pd_tprofessional_preferences()
	{
		return $this->hasMany(\App\Models\PdTprofessionalPreference::class, 'fkpreference');
	}
}
