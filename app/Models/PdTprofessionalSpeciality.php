<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 08:56:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PdTprofessionalSpeciality
 * 
 * @property int $fkprofessional
 * @property int $fkspecialty
 * 
 * @property \App\Models\PdTprofessional $pd_tprofessional
 * @property \App\Models\PdTspeciality $pd_tspeciality
 *
 * @package App\Models
 */
class PdTprofessionalSpeciality extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'fkprofessional' => 'int',
		'fkspecialty' => 'int'
	];

	public function pd_tprofessional()
	{
		return $this->belongsTo(\App\Models\PdTprofessional::class, 'fkprofessional');
	}

	public function pd_tspeciality()
	{
		return $this->belongsTo(\App\Models\PdTspeciality::class, 'fkspecialty');
	}
}
