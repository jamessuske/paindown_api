<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 08:56:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PdTcity
 * 
 * @property int $pkcity
 * @property string $city_name
 * @property float $city_latitude
 * @property string $city_longitude
 * @property \Carbon\Carbon $create_at
 * @property \Carbon\Carbon $update_at
 * @property int $fkregion
 * 
 * @property \App\Models\PdTregion $pd_tregion
 * @property \Illuminate\Database\Eloquent\Collection $pd_tpatients
 * @property \Illuminate\Database\Eloquent\Collection $pd_tprofessionals
 *
 * @package App\Models
 */
class PdTcity extends Eloquent
{
	protected $table = 'pd_tcity';
	protected $primaryKey = 'pkcity';
	public $timestamps = false;

	protected $casts = [
		'city_latitude' => 'float',
		'fkregion' => 'int'
	];

	protected $dates = [
		'create_at',
		'update_at'
	];

	protected $fillable = [
		'city_name',
		'city_latitude',
		'city_longitude',
		'create_at',
		'update_at',
		'fkregion'
	];

	public function pd_tregion()
	{
		return $this->belongsTo(\App\Models\PdTregion::class, 'fkregion');
	}

	public function pd_tpatients()
	{
		return $this->hasMany(\App\Models\PdTpatient::class, 'fkcity');
	}

	public function pd_tprofessionals()
	{
		return $this->hasMany(\App\Models\PdTprofessional::class, 'fkcity');
	}
}
