<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 08:56:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PdTlogin
 * 
 * @property int $id
 * @property string $email
 * @property string $password
 * @property int $fkpatient
 * @property int $fkadmin
 * @property int $fkrol
 * @property int $active
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $fkprofessional
 * 
 * @property \App\Models\PdTpatient $pd_tpatient
 * @property \App\Models\PdRole $pd_role
 * @property \App\Models\PdTadmin $pd_tadmin
 * @property \App\Models\PdTprofessional $pd_tprofessional
 *
 * @package App\Models
 */
class PdTlogin extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'pd_tlogin';

	protected $casts = [
		'fkpatient' => 'int',
		'fkadmin' => 'int',
		'fkrol' => 'int',
		'active' => 'int',
		'fkprofessional' => 'int'
	];

	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $fillable = [
		'email',
		'password',
		'fkpatient',
		'fkrol',
		'active'
	];

	public function pd_tpatient()
	{
		return $this->belongsTo(\App\Models\PdTpatient::class, 'fkpatient');
	}

	public function pd_role()
	{
		return $this->belongsTo(\App\Models\PdRole::class, 'fkrol');
	}

	public function pd_tadmin()
	{
		return $this->belongsTo(\App\Models\PdTadmin::class, 'fkadmin');
	}

	public function pd_tprofessional()
	{
		return $this->belongsTo(\App\Models\PdTprofessional::class, 'fkprofessional');
	}
}
