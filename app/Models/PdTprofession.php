<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 08:56:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PdTprofession
 * 
 * @property int $pkprofession
 * @property string $profession_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $pd_tprofessionals
 *
 * @package App\Models
 */
class PdTprofession extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $primaryKey = 'pkprofession';

	protected $fillable = [
		'profession_name'
	];

	public function pd_tprofessionals()
	{
		return $this->hasMany(\App\Models\PdTprofessional::class, 'fkprofession');
	}
}
