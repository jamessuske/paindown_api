<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 08:56:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PdRole
 * 
 * @property int $pkrol
 * @property string $rol_shortname
 * @property string $rol_fullname
 * 
 * @property \Illuminate\Database\Eloquent\Collection $pd_tlogins
 *
 * @package App\Models
 */
class PdRole extends Eloquent
{
	protected $table = 'pd_role';
	protected $primaryKey = 'pkrol';
	public $timestamps = false;

	protected $fillable = [
		'rol_shortname',
		'rol_fullname'
	];

	public function pd_tlogins()
	{
		return $this->hasMany(\App\Models\PdTlogin::class, 'fkrol');
	}
}
