<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 08:56:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PdTpatientProfessionalChat
 * 
 * @property int $pkpatientchat
 * @property string $patient_professional_chat_url
 * @property int $patient_professional_chat_readprof
 * @property int $patient_professional_chat_readpat
 * @property int $patient_professional_chat_noread
 * @property int $fkprofessional
 * @property int $fkpatient
 * 
 * @property \App\Models\PdTpatient $pd_tpatient
 * @property \App\Models\PdTprofessional $pd_tprofessional
 *
 * @package App\Models
 */
class PdTpatientProfessionalChat extends Eloquent
{
	protected $table = 'pd_tpatient_professional_chat';
	protected $primaryKey = 'pkpatientchat';
	public $timestamps = false;

	protected $casts = [
		'patient_professional_chat_readprof' => 'int',
		'patient_professional_chat_readpat' => 'int',
		'patient_professional_chat_noread' => 'int',
		'fkprofessional' => 'int',
		'fkpatient' => 'int'
	];

	protected $fillable = [
		'patient_professional_chat_url',
		'patient_professional_chat_readprof',
		'patient_professional_chat_readpat',
		'patient_professional_chat_noread',
		'fkprofessional',
		'fkpatient'
	];

	public function pd_tpatient()
	{
		return $this->belongsTo(\App\Models\PdTpatient::class, 'fkpatient');
	}

	public function pd_tprofessional()
	{
		return $this->belongsTo(\App\Models\PdTprofessional::class, 'fkprofessional');
	}
}
