<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 08:56:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PdPatientPainkiller
 * 
 * @property int $fkpatient
 * @property int $fkpainkiller
 * 
 * @property \App\Models\PdTpatient $pd_tpatient
 * @property \App\Models\PdTpainkiller $pd_tpainkiller
 *
 * @package App\Models
 */
class PdPatientPainkiller extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'fkpatient' => 'int',
		'fkpainkiller' => 'int'
	];

	public function pd_tpatient()
	{
		return $this->belongsTo(\App\Models\PdTpatient::class, 'fkpatient');
	}

	public function pd_tpainkiller()
	{
		return $this->belongsTo(\App\Models\PdTpainkiller::class, 'fkpainkiller');
	}
}
