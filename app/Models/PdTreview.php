<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 08:56:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PdTreview
 * 
 * @property int $pkreview
 * @property string $review_description
 * @property \Carbon\Carbon $create_at
 * @property \Carbon\Carbon $update_at
 * @property int $review_value
 * @property int $fkprofessional
 * @property int $fkpatient
 * 
 * @property \App\Models\PdTpatient $pd_tpatient
 * @property \App\Models\PdTprofessional $pd_tprofessional
 *
 * @package App\Models
 */
class PdTreview extends Eloquent
{
	use SoftDeletes;

	protected $primaryKey = 'pkreview';
	public $timestamps = true;

	protected $casts = [
		'review_value' => 'int',
		'fkprofessional' => 'int',
		'fkpatient' => 'int'
	];

	protected $fillable = [
		'review_description',
		'create_at',
		'update_at',
		'review_value',
		'fkprofessional',
		'fkpatient'
	];

	public function pd_tpatient()
	{
		return $this->belongsTo(\App\Models\PdTpatient::class, 'fkpatient');
	}

	public function pd_tprofessional()
	{
		return $this->belongsTo(\App\Models\PdTprofessional::class, 'fkprofessional');
	}
}
