<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 08:56:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PdTprofessionalPreference
 * 
 * @property int $fkpreference
 * @property int $patientpreference_selected
 * @property int $fkprofessional
 * 
 * @property \App\Models\PdTpreference $pd_tpreference
 * @property \App\Models\PdTprofessional $pd_tprofessional
 *
 * @package App\Models
 */
class PdTprofessionalPreference extends Eloquent
{
	protected $table = 'pd_tprofessional_preference';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'fkpreference' => 'int',
		'patientpreference_selected' => 'int',
		'fkprofessional' => 'int'
	];

	protected $fillable = [
		'patientpreference_selected'
	];

	public function pd_tpreference()
	{
		return $this->belongsTo(\App\Models\PdTpreference::class, 'fkpreference');
	}

	public function pd_tprofessional()
	{
		return $this->belongsTo(\App\Models\PdTprofessional::class, 'fkprofessional');
	}
}
