<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 08:56:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PdTmembershipControl
 * 
 * @property int $pkmembershipcontrol
 * @property int $membership_control
 * @property string $membership_value
 * @property int $fkmembership
 * 
 * @property \App\Models\PdTmembership $pd_tmembership
 *
 * @package App\Models
 */
class PdTmembershipControl extends Eloquent
{
	protected $table = 'pd_tmembership_control';
	protected $primaryKey = 'pkmembershipcontrol';
	public $timestamps = false;

	protected $casts = [
		'membership_control' => 'int',
		'fkmembership' => 'int'
	];

	protected $fillable = [
		'membership_control',
		'membership_value',
		'fkmembership'
	];

	public function pd_tmembership()
	{
		return $this->belongsTo(\App\Models\PdTmembership::class, 'fkmembership');
	}
}
