<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 08:56:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PdTpainkiller
 * 
 * @property int $pkpainkiller
 * @property string $painkiller_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $pd_patient_painkillers
 *
 * @package App\Models
 */
class PdTpainkiller extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	//protected $primaryKey = 'pkpainkiller';
	protected $table = 'pd_tpainkillers';
        protected $primaryKey = 'pkpainkiller';

	protected $fillable = [
		'painkiller_name'
	];

	/*public function pd_patient_painkillers()
	{
		return $this->hasMany(\App\Models\PdPatientPainkiller::class, 'fkpainkiller');
	}*/

	public function pd_tpatients()
        {
                return $this->hasMany(\App\Models\PdTpatient::class, 'patient_usagepainkiller');
        }
}
