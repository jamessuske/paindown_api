<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 08:56:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PdTcountry
 * 
 * @property int $pkcountry
 * @property string $country_code
 * @property string $country_name
 * @property string $country_localName
 * @property string $country_webCode
 * @property string $country_region
 * @property string $country_continent
 * @property float $country_latitude
 * @property float $country_longitude
 * @property string $country_surfaceArea
 * @property string $country_population
 * @property string $country_postcodeexpression
 * @property \Carbon\Carbon $create_at
 * @property \Carbon\Carbon $update_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $pd_tregions
 *
 * @package App\Models
 */
class PdTcountry extends Eloquent
{
	protected $table = 'pd_tcountry';
	protected $primaryKey = 'pkcountry';
	public $timestamps = false;

	protected $casts = [
		'country_latitude' => 'float',
		'country_longitude' => 'float'
	];

	protected $dates = [
		'create_at',
		'update_at'
	];

	protected $fillable = [
		'country_code',
		'country_name',
		'country_localName',
		'country_webCode',
		'country_region',
		'country_continent',
		'country_latitude',
		'country_longitude',
		'country_surfaceArea',
		'country_population',
		'country_postcodeexpression',
		'create_at',
		'update_at'
	];

	public function pd_tregions()
	{
		return $this->hasMany(\App\Models\PdTregion::class, 'fkcountry');
	}
}
