<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 04 May 2018 06:30:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PdTpatient
 * 
 * @property int $pkpatient
 * @property string $patient_username
 * @property string $patient_image
 * @property string $patient_address
 * @property string $patient_painyears
 * @property int $fkpainlevel
 * @property int $fkpaintype
 * @property int $fkcity
 * @property int $patient_usagepainkiller
 * @property string $patient_otherpainkillers
 * @property string $patient_painstory
 * @property int $patient_painstorypublic
 * @property int $fkmembership
 * @property \Carbon\Carbon $patient_membershipvalidity
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\PdTmembership $pd_tmembership
 * @property \App\Models\PdTcity $pd_tcity
 * @property \App\Models\PdTpainlevel $pd_tpainlevel
 * @property \App\Models\PdTpaintype $pd_tpaintype
 * @property \Illuminate\Database\Eloquent\Collection $pd_patient_painkillers
 * @property \Illuminate\Database\Eloquent\Collection $pd_tlogins
 * @property \Illuminate\Database\Eloquent\Collection $pd_tpatient_chats
 * @property \Illuminate\Database\Eloquent\Collection $pd_tpatient_painkillers
 * @property \Illuminate\Database\Eloquent\Collection $pd_tpatient_preferences
 * @property \Illuminate\Database\Eloquent\Collection $pd_tpatient_professional_chats
 * @property \Illuminate\Database\Eloquent\Collection $pd_treviews
 *
 * @package App\Models
 */
class PdTpatient extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'pd_tpatient';
	protected $primaryKey = 'pkpatient';

	protected $casts = [
		'fkpainlevel' => 'int',
		'fkpaintype' => 'int',
		'fkcountry' => 'int',
		'fkregion' => 'int',
		'fkcity' => 'int',
		'patient_usagepainkiller' => 'int',
		'patient_painstorypublic' => 'int',
		'fkmembership' => 'int'
	];

	protected $dates = [
		'patient_membershipvalidity'
	];

	protected $fillable = [
		'patient_username',
		'patient_image',
		'patient_address',
		'patient_painyears',
		'fkpainlevel',
		'fkpaintype',
		'fkcountry',
		'fkregion',
		'fkcity',
		'patient_usagepainkiller',
		'patient_otherpainkillers',
		'patient_painstory',
		'patient_painstorypublic',
		'fkmembership'
	];

	public function pd_tmembership()
	{
		return $this->belongsTo(\App\Models\PdTmembership::class, 'fkmembership');
	}

	public function pd_tcountry()
	{
		return $this->belongsTo(\App\Models\PdTcountry::class, 'fkcountry');
	}
	
	public function pd_tregion()
	{
		return $this->belongsTo(\App\Models\PdTregion::class, 'fkregion');
	}
	
	public function pd_tcity()
	{
		return $this->belongsTo(\App\Models\PdTcity::class, 'fkcity');
	}

	public function pd_tpainlevel()
	{
		return $this->belongsTo(\App\Models\PdTpainlevel::class, 'fkpainlevel');
	}

	public function pd_tpaintype()
	{
		return $this->belongsTo(\App\Models\PdTpaintype::class, 'fkpaintype');
	}

	public function pd_tpainkiller()
	{
		return $this->belongsTo(\App\Models\PdTpainkiller::class, 'patient_usagepainkiller');
	}

	public function pd_patient_painkillers()
	{
		return $this->hasMany(\App\Models\PdPatientPainkiller::class, 'fkpatient');
	}

	public function pd_tlogins()
	{
		return $this->hasMany(\App\Models\PdTlogin::class, 'fkpatient');
	}

	public function pd_tpatient_chats()
	{
		return $this->hasMany(\App\Models\PdTpatientChat::class, 'fkpatient2');
	}

	public function pd_tpatient_painkillers()
	{
		return $this->hasMany(\App\Models\PdTpatientPainkiller::class, 'fkpatient');
	}

	public function pd_tpatient_preferences()
	{
		return $this->hasMany(\App\Models\PdTpatientPreference::class, 'fkpatient');
	}

	public function pd_tpatient_professional_chats()
	{
		return $this->hasMany(\App\Models\PdTpatientProfessionalChat::class, 'fkpatient');
	}

	public function pd_treviews()
	{
		return $this->hasMany(\App\Models\PdTreview::class, 'fkpatient');
	}
}
