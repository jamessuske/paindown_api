<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 08:56:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PdTpatientChat
 * 
 * @property int $pkpatientchat
 * @property string $patientchat_url
 * @property int $fkpatient1
 * @property int $fkpatient2
 * @property int $patientchat_read1
 * @property int $patientchat_read2
 * @property int $patientchat_noread
 * 
 * @property \App\Models\PdTpatient $pd_tpatient
 *
 * @package App\Models
 */
class PdTpatientChat extends Eloquent
{
	protected $table = 'pd_tpatient_chat';
	protected $primaryKey = 'pkpatientchat';
	public $timestamps = false;

	protected $casts = [
		'fkpatient1' => 'int',
		'fkpatient2' => 'int',
		'patientchat_read1' => 'int',
		'patientchat_read2' => 'int',
		'patientchat_noread' => 'int'
	];

	protected $fillable = [
		'patientchat_url',
		'fkpatient1',
		'fkpatient2',
		'patientchat_read1',
		'patientchat_read2',
		'patientchat_noread'
	];

	public function pd_tpatient()
	{
		return $this->belongsTo(\App\Models\PdTpatient::class, 'fkpatient2');
	}
}
