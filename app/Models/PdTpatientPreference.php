<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 08:56:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PdTpatientPreference
 * 
 * @property int $fkpreference
 * @property int $fkpatient
 * @property int $patientpreference_selected
 * 
 * @property \App\Models\PdTpatient $pd_tpatient
 * @property \App\Models\PdTpreference $pd_tpreference
 *
 * @package App\Models
 */
class PdTpatientPreference extends Eloquent
{
	protected $table = 'pd_tpatient_preference';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'fkpreference' => 'int',
		'fkpatient' => 'int',
		'patientpreference_selected' => 'int'
	];

	protected $fillable = [
		'patientpreference_selected'
	];

	public function pd_tpatient()
	{
		return $this->belongsTo(\App\Models\PdTpatient::class, 'fkpatient');
	}

	public function pd_tpreference()
	{
		return $this->belongsTo(\App\Models\PdTpreference::class, 'fkpreference');
	}
}
