<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 08:56:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PdTregion
 * 
 * @property int $pkregion
 * @property string $region_name
 * @property float $region_latitude
 * @property float $region_longitude
 * @property \Carbon\Carbon $create_at
 * @property \Carbon\Carbon $update_at
 * @property int $fkcountry
 * 
 * @property \App\Models\PdTcountry $pd_tcountry
 * @property \Illuminate\Database\Eloquent\Collection $pd_tcities
 *
 * @package App\Models
 */
class PdTregion extends Eloquent
{
	protected $table = 'pd_tregion';
	protected $primaryKey = 'pkregion';
	public $timestamps = false;

	protected $casts = [
		'region_latitude' => 'float',
		'region_longitude' => 'float',
		'fkcountry' => 'int'
	];

	protected $dates = [
		'create_at',
		'update_at'
	];

	protected $fillable = [
		'region_name',
		'region_latitude',
		'region_longitude',
		'create_at',
		'update_at',
		'fkcountry'
	];

	public function pd_tcountry()
	{
		return $this->belongsTo(\App\Models\PdTcountry::class, 'fkcountry');
	}

	public function pd_tcities()
	{
		return $this->hasMany(\App\Models\PdTcity::class, 'fkregion');
	}
}
