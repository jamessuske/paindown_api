<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Lib\Util;
use JWTAuth;
use App\Models\PdTlogin;
use App\Models\PdTprofessional;
use App\Models\PdTpatient;
use App\Models\PdTprofessionalQualifcation;
use App\Models\PdTprofessionalSpeciality;
use App\Models\PdTpatientPainkiller;
use App\Models\PdTpatientPreference;
use App\Models\PdTprofessionalPreference;
use App\Models\PdTreview;
use Jcf\Geocode\Geocode;
use Illuminate\Foundation\Auth\ResetsPasswords;

class UserController extends Controller
{


     use ResetsPasswords;


     protected function sendResetResponse($response)
     {
         return response()->json(['success' => trans($response)]);
     }
     
     protected function sendResetFailedResponse(Request $request, $response)
     {
         return response()->json(['error' => trans($response)], 401);
     }
     
     protected function rules()
     {
        return [
            'email' => 'required|email',
            'newpassword' => 'required|confirmed',
        ];
     }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function getUserByEmail($email)
    {
	    $tmp = PdTlogin::where('email', $email)->first();
	    if($tmp){
		return $tmp;
	    }
	    else
	    {
                return ['error'=>true,'User not found!'];
	    }
    }

    public function getUserByUsername($username, $order)
    {

        $tmp = PdTpatient::where('patient_username', $username)->first();
        if($tmp){
            $userLogin = $tmp->pd_tlogins()->first();
            $city = $userLogin->pd_tpatient()->first()->pd_tcity()->first();
            $region = $city->pd_tregion()->first();
            $country = $region->pd_tcountry()->select('pkcountry as id', 'country_name as name')->first();
            $user['state'] = 2;
            $user['username'] = $tmp->patient_username;
            $user['image'] = $tmp->patient_image;
            $user['address'] = $tmp->patient_address;
            $user['pain_years'] = $tmp->patient_painyears;
            $user['pain_level'] = $tmp->pd_tpainlevel()->first();
            $user['pain_type'] = $tmp->pd_tpaintype()->first();
            $user['location'] = [
                'city' => ['id'=>$city->pkcity, 'name'=> $city->city_name],
                'region' => ['id'=>$region->pkregion, 'name'=> $region->region_name],
                'country' => $country
            ];
            $user['painkillers'] = PdTpatientPainkiller::join('pd_tpainkillers', 'pd_tpainkillers.pkpainkiller', '=', 'pd_tpatient_painkiller.fkpainkiller')->select('pd_tpainkillers.pkpainkiller as id', 'pd_tpainkillers.painkiller_name as name')->where('pd_tpatient_painkiller.fkpatient', $tmp->pkpatient)->get();
            $user['email'] = $userLogin->email;
            $user['usage_painkiller'] = $tmp->patient_usagepainkiller;
	    $user['patient_painkiller'] = $tmp->pd_tpainkiller()->first()->painkiller_name;
            $user['other_painkiller'] = $tmp->patient_otherpainkillers;
            $user['story'] = $tmp->patient_painstory;
            $user['public'] = $tmp->patient_painstorypublic;
            $user['membership'] = $tmp->fkmembership;
            $user['membership_date'] = $tmp->patient_membershipvalidity;
            return $user;
        }else{
            $tmp = PdTprofessional::where('professional_username', $username)->first();
            if($tmp){
                $userLogin = $tmp->pd_tlogins()->first();
                $city = $tmp->pd_tcity()->first();
                $region = $city->pd_tregion()->first();
                $country = $region->pd_tcountry()->select('pkcountry as id', 'country_name as name')->first();
                $user['username'] = $tmp->professional_username;
                $user['state'] = 3;
                $user['name'] = $tmp->professional_name;
                $user['image'] = $tmp->professional_image;
                //if($tmp->fkmembership == 2){
                    $user['address'] = $tmp->professional_address;
                    $user['phone'] = $tmp->professional_phone;
                    $user['website'] = $tmp->professional_website;
                    $user['email'] = $userLogin->email;
                //}
                $user['profession'] = $tmp->pd_tprofession()->first();
                $user['about'] = $tmp->professional_about;
                $user['approach'] = $tmp->professional_approach;
                $user['stories'] = $tmp->professional_stories;
                $user['membership'] = $tmp->fkmembership;
		$user['rating'] = $tmp->professional_rating;
                $user['location'] = [
                    'city' => ['id'=>$city->pkcity, 'name'=> $city->city_name],
                    'region' => ['id'=>$region->pkregion, 'name'=> $region->region_name],
                    'country' => $country
                ];

                $user['qualifications'] = PdTprofessionalQualifcation::
                join('pd_tqualifications', 'pd_tqualifications.pkqualification', '=', 'pd_tprofessional_qualifcations.fkqualification')
                ->select('pd_tqualifications.pkqualification as id', 'pd_tqualifications.qualification_name as name')
                ->where('pd_tprofessional_qualifcations.fkprofessional', $tmp->pkprofessional)->get();

		if($order == "1")
		{
			$user['reviews'] = PdTreview::where('pd_treviews.review_to', $userLogin->id)->orderBy('pd_treviews.review_value', 'DESC')->get();
		}
		else
		{
			$user['reviews'] = PdTreview::where('pd_treviews.review_to', $userLogin->id)->orderBy('pd_treviews.pkreview', 'DESC')->get();
		}

		$reviewCount = 0;
		foreach($user['reviews'] as $review)
		{
			
			$fromUser = PdTlogin::where('id', $user['reviews'][$reviewCount]['review_from'])->first(); 
			$reviewTmp = PdTpatient::where('pkpatient', $fromUser->fkpatient)->first();
			if($reviewTmp)
			{
				$user['reviews'][$reviewCount]['from_username'] = $reviewTmp->patient_username;	
			}
			else
			{
				$reviewTmp = PdTprofessional::where('pkprofessional', $fromUser->fkprofessional)->first();
				$user['reviews'][$reviewCount]['from_username'] = $reviewTmp->professional_username;	
			}
			$reviewCount++;
		}



		$user['specialities'] = PdTprofessionalSpeciality::
                join('pd_tspecialities', 'pd_tspecialities.pkspecialty', '=', 'pd_tprofessional_specialities.fkspecialty')
                ->select('pd_tspecialities.pkspecialty as id', 'pd_tspecialities.specialty_name as name')
                ->where('pd_tprofessional_specialities.fkprofessional', $tmp->pkprofessional)->get();
                return $user;
            }else
                return ['error'=>true,'User not found!'];
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $tmp = PdTpatient::where('patient_username', $id)->first();
        if($tmp){
            $userLogin = $tmp->pd_tlogins()->first();
            $city = $userLogin->pd_tpatient()->first()->pd_tcity()->first();
            $region = $city->pd_tregion()->first();
            $country = $region->pd_tcountry()->select('pkcountry as id', 'country_name as name')->first();
            $user['state'] = 2;
            $user['username'] = $tmp->patient_username;
            $user['image'] = $tmp->patient_image;
            $user['address'] = $tmp->patient_address;
            $user['pain_years'] = $tmp->patient_painyears;
            $user['pain_level'] = $tmp->pd_tpainlevel()->first();
            $user['pain_type'] = $tmp->pd_tpaintype()->first();
            $user['location'] = [
                'city' => ['id'=>$city->pkcity, 'name'=> $city->city_name],
                'region' => ['id'=>$region->pkregion, 'name'=> $region->region_name],
                'country' => $country
            ];
            $user['painkillers'] = PdTpatientPainkiller::
            join('pd_tpainkillers', 'pd_tpainkillers.pkpainkiller', '=', 'pd_tpatient_painkiller.fkpainkiller')
            ->select('pd_tpainkillers.pkpainkiller as id', 'pd_tpainkillers.painkiller_name as name')
            ->where('pd_tpatient_painkiller.fkpatient', $tmp->pkpatient)->get();
            $user['email'] = $userLogin->email;
            $user['usage_painkiller'] = $tmp->patient_usagepainkiller;
            $user['other_painkiller'] = $tmp->patient_otherpainkillers;
            $user['story'] = $tmp->patient_painstory;
            $user['public'] = $tmp->patient_painstorypublic;
            $user['membership'] = $tmp->fkmembership;
            $user['membership_date'] = $tmp->patient_membershipvalidity;
            return $user;
        }else{
            $tmp = PdTprofessional::where('professional_username', $id)->first();
            if($tmp){
                $userLogin = $tmp->pd_tlogins()->first();
                $city = $tmp->pd_tcity()->first();
                $region = $city->pd_tregion()->first();
                $country = $region->pd_tcountry()->select('pkcountry as id', 'country_name as name')->first();
                $user['username'] = $tmp->professional_username;
                $user['state'] = 3;
                $user['name'] = $tmp->professional_name;
                $user['image'] = $tmp->professional_image;
                //if($tmp->fkmembership == 2){
                    $user['address'] = $tmp->professional_address;
                    $user['phone'] = $tmp->professional_phone;
                    $user['website'] = $tmp->professional_website;
                    $user['email'] = $userLogin->email;
                //}
                $user['profession'] = $tmp->pd_tprofession()->first();
                $user['about'] = $tmp->professional_about;
                $user['approach'] = $tmp->professional_approach;
                $user['stories'] = $tmp->professional_stories;
                $user['membership'] = $tmp->fkmembership;
                $user['location'] = [
                    'city' => ['id'=>$city->pkcity, 'name'=> $city->city_name],
                    'region' => ['id'=>$region->pkregion, 'name'=> $region->region_name],
                    'country' => $country
                ];

                $user['qualifications'] = PdTprofessionalQualifcation::
                join('pd_tqualifications', 'pd_tqualifications.pkqualification', '=', 'pd_tprofessional_qualifcations.fkqualification')
                ->select('pd_tqualifications.pkqualification as id', 'pd_tqualifications.qualification_name as name')
                ->where('pd_tprofessional_qualifcations.fkprofessional', $tmp->pkprofessional)->get();

		//if($request->review_order == "1")
		//{
			//$user['reviews'] = PdTreview::where('pd_treviews.review_to', $userLogin->id)->orderBy('pd_treviews.created_at', 'DESC')->get();
		//}
		//else
		//{
			$user['reviews'] = PdTreview::where('pd_treviews.review_to', $userLogin->id)->orderBy('pd_treviews.pkreview', 'DESC')->get();
		//}

		$reviewCount = 0;
		foreach($user['reviews'] as $review)
		{
			
			$fromUser = PdTlogin::where('id', $user['reviews'][$reviewCount]['review_from'])->first(); 
			$reviewTmp = PdTpatient::where('pkpatient', $fromUser->fkpatient)->first();
			if($reviewTmp)
			{
				$user['reviews'][$reviewCount]['from_username'] = $reviewTmp->patient_username;	
			}
			else
			{
				$reviewTmp = PdTprofessional::where('pkprofessional', $fromUser->fkprofessional)->first();
				$user['reviews'][$reviewCount]['from_username'] = $reviewTmp->professional_username;	
			}
			$reviewCount++;
		}



		$user['specialities'] = PdTprofessionalSpeciality::
                join('pd_tspecialities', 'pd_tspecialities.pkspecialty', '=', 'pd_tprofessional_specialities.fkspecialty')
                ->select('pd_tspecialities.pkspecialty as id', 'pd_tspecialities.specialty_name as name')
                ->where('pd_tprofessional_specialities.fkprofessional', $tmp->pkprofessional)->get();
                return $user;
            }else
                return ['error'=>true,'User not found!'];
        }
    }

    public function updatePassword(Request $request)
    {
	$credentials = $request->only('email', 'password');
	$token = JWTAuth::attempt($credentials);
        $token = JWTAuth::parseToken()->authenticate();
	$userLogin = PdTlogin::where('id', $token->id)->first();
	$userLogin->email = $token->email;
	$userLogin->password = bcrypt($request->newpassword);
	$userLogin->save();
	return Util::getSuccess();
    }


    	public function updatePerference(Request $request)
	{
		$token = JWTAuth::parseToken()->authenticate();
		$userLogin = PdTlogin::where('id', $token->id)->first();
		if ($userLogin->fkpatient != null) {
			$user = $userLogin->pd_tpatient()->first();
			PdTpatientPreference::where('fkpatient', $user->pkpatient)->delete();
                        foreach($request->all() as $p)
                        {
                                $perference = new PdTpatientPreference;
                                $perference->fkpreference = $p["pkpreference"];
                                $perference->fkpatient = $user->pkpatient;
                                $perference->patientpreference_selected = $p["patientpreference_selected"];
                                $perference->save();
                        }
			return Util::getSuccess();
		}
		if($userLogin->fkprofessional != null){
			$user = $userLogin->pd_tprofessional()->first();
			PdTprofessionalPreference::where('fkprofessional', $user->pkprofessional)->delete();
                        foreach($request->all() as $p)
                        {
                                $perference = new PdTprofessionalPreference;
                                $perference->fkpreference = $p["pkpreference"];
                                $perference->fkprofessional = $user->pkprofessional;
                                $perference->patientpreference_selected = $p["patientpreference_selected"];
                                $perference->save();
                        }
			return Util::getSuccess();
		}
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $token = JWTAuth::parseToken()->authenticate();
        /*$rules = [
            'firstname'      => 'required|min:3',
            'lastname'     => 'required',
            'phone'  => 'required|numeric',
            'email'  => 'required|email',
            'state' =>  'required|numeric|between:2,3',
            'news' => 'required|boolean',
            'sms' => 'required|boolean',
        ];

        $validator = \Validator::make($request->all(), $rules);


        if ($validator->fails()) {
            return [
                'status' => false,
                'message' => 'the request could not be processed',
                'errors'  => $validator->errors()->all()
            ];
        }*/
        $userLogin = PdTlogin::where('id', $token->id)->first();
        if ($userLogin->fkpatient != null) {
            $user = $userLogin->pd_tpatient()->first();
            if($user){
                $user->patient_address = $request->address;
                $user->fkcity = $request->location["city"]["id"];
		$user->fkregion = $request->location["region"]["id"];
		$user->fkcountry = $request->location["country"]["id"];
                $user->fkpainlevel = $request->pain_level;
                $user->fkpaintype = $request->pain_type;
                $user->patient_painyears = $request->pain_years;
                $user->patient_painstorypublic = $request->public;
                $user->patient_painstory = $request->story;
                $user->patient_usagepainkiller = $request->usage_painkiller;
		$user->patient_otherpainkillers = $request->other_painkiller;
		$user->patient_image = $request->patient_image;
		$response = Geocode::make()->address($request->location["city"]["name"] . ", " . $request->location["region"]["name"]);
		if ($response) {
			$user->patient_latitude = $response->latitude();
			$user->patient_longitude = $response->longitude();
		}

		if( $user->save() ) {

                   	return Util::getSuccess();

		}
		else
		{
                    	return Util::getErrors();
		}
            }
        }
        if($userLogin->fkprofessional != null){
            $user = $userLogin->pd_tprofessional()->first();
            if($user){
                $user->professional_about = $request->about;
                $user->professional_address = $request->address;
                $user->professional_approach = $request->approach;
                $user->fkprofession = $request->profession;
                $user->professional_stories = $request->stories;
		$user->professional_website = $request->website;
		$user->professional_image = $request->professional_image;
                $user->fkcity = $request->location["city"]["id"];
		$user->fkregion = $request->location["region"]["id"];
		$user->fkcountry = $request->location["country"]["id"];
		$response = Geocode::make()->address($request->location["city"]["name"] . ", " . $request->location["region"]["name"]);
		if ($response) {
			$user->professional_latitude = $response->latitude();
			$user->professional_longitude = $response->longitude();
		}
                if( $user->save()){
			PdTprofessionalSpeciality::where('fkprofessional', $user->pkprofessional)->delete();
			PdTprofessionalQualifcation::where('fkprofessional', $user->pkprofessional)->delete();
			foreach($request->specialities as $s)
			{
				$speciality = new PdTprofessionalSpeciality;
                                $speciality->fkspecialty = $s["id"];
                                $speciality->fkprofessional = $user->pkprofessional;
                                $speciality->save();
			}
			foreach($request->qualifications as $q)
			{
				$qualification = new PdTprofessionalQualifcation;
                                $qualification->fkqualification = $q["id"];
                                $qualification->fkprofessional = $user->pkprofessional;
                                $qualification->save();
			}
                    	return Util::getSuccess();
		}else{
			return Util::getErrors();
		}
            }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
