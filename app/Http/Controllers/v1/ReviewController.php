<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Lib\Util;
use Illuminate\Support\Facades\DB;
use App\Models\PdTreview;
use App\Models\PdTprofessional;
use App\Models\PdTlogin;
use JWTAuth;

class ReviewController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return PdTproblemsReviews::select('pkproblem as id', 'problem_name as text')->get();
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $items = [];
        $token = JWTAuth::parseToken()->authenticate();
        $userLogin = PdTlogin::where('id', $token->id)->first();
        $prof = PdTprofessional::where('professional_username', $request->user)->first();
        if($prof){
            $review = new PdTreview;
            $review->fkproblem = $request->problem;
            $review->review_description = $request->text;
            $review->review_value = floatval($request->rating);
            $review->review_from = $token->id;
            $review->review_to = $prof->pd_tlogins()->first()->id;
            if($review->save()){
                $sum = 0;
                $reviews = PdTreview::where('review_to', $review->review_to)->get();
                foreach ($reviews as $_review) {
                    /*$from = PdTlogin::where('id', $review->review_from)->first();
                    $r = [];
                    $r['value'] = $_review->review_value;
                    $r['description'] = $_review->review_description;
                    $query = "select * from pd_tproblems_reviews where pkproblem = " . $_review->fkproblem;
                    $r['problem'] = DB::select(DB::raw($query))[0];
                    $r['user'] = $from->pd_tpatient()->first()->patient_username;
		    $r['date'] = $_review->created_at;*/
                    $sum = floatval($sum) + floatval($_review->review_value);
                    //array_push($items, $r);
                }
                $prof->professional_rating = number_format($sum / count($reviews), 1);
                $prof->save();
		return Util::getSuccess();
            }
	    else
	    {
		    return Util::getErrors();
	    }
	}
	return Util::getErrors();
	//return json_encode($request->text);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($username)
    {
        $items = [];
        $prof = PdTprofessional::where('professional_username', '=', $username)->first();
        if($prof){
            $reviews = PdTreview::where('review_to', $prof->pd_tlogins()->first()->id)->get();
            foreach ($reviews as $_review) {
                $from = PdTlogin::where('id', $_review->review_from)->first();
                $r = [];
                $r['value'] = floatval($_review->review_value);
                $r['description'] = $_review->review_description;
                $query = "select * from pd_tproblems_reviews where pkproblem = " . $_review->fkproblem;
                $r['problem'] = DB::select(DB::raw($query))[0];
                $r['user'] = $from->pd_tpatient()->first()->patient_username;
                $r['date'] = $_review->created_at;
                $r['id'] = $_review->pkreview;
                array_push($items, $r);
            }
        }
        return $items;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
