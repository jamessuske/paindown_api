<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\PdTlogin;
use App\Models\PdTprofessional;
use App\Models\PdTpatientPainkiller;
use App\Models\PdTprofessionalQualifcation;
use App\Models\PdTprofessionalSpeciality;
use App\Models\PdTprofessionalPreference;
use App\Models\PdTpatientPreference;
use App\Models\PdTpreference;
use Config;
use LbTtradesman_login;
use Auth;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class AuthController extends Controller
{


   use SendsPasswordResetEmails;

   public function resetPassword(request $request)
   {
	echo "Here";
	//$this->reset($request);
	return response()->json($request);
   }

    public function sendPasswordEmail(request $request)
    {
	$userLogin = PdTlogin::where('email', $request->email)->first();
        if($userLogin !== NULL){
            $this->sendResetLinkEmail($request);
            $data = [
            	'response'=> 'Email has been sent! Please follow the instructions in the email to reset your password.'
            ];
        }
	return response()->json($data);
    }


    public function login(Request $request)
    {
        // credenciales para loguear al usuario
        $credentials = $request->only('email', 'password');
        $token = JWTAuth::attempt($credentials);

        try {
            // si los datos de login no son correctos
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'credenciales invalidas'], 401);
            }
        } catch (JWTException $e) {
            // si no se puede crear el token
            return response()->json(['error' => 'No puede crear el token'], 500);
        }
        $userLogin = PdTlogin::where('email', $request->email)->first();

        $user =[];
        if($userLogin->fkadmin != null){
            $tmp = $userLogin->pd_tadmin()->first();
            $user['id'] =$userLogin->id;
            $user['name'] = $tmp->admin_name;
            $user['state']= 1;        
        }

        if($userLogin->fkpatient != null){
            $userLogin->pd_tpatient()->first();
            $user['state']= 2;
            //$city = $userLogin->pd_tpatient()->first()->pd_tcity()->first();
            //$user['id'] =$userLogin->id;
            //$user['city'] = $city->city_name;
            //$user['country'] = $city->pd_tregion()->first()->pd_tcountry()->first()->country_name;
        }

        if($userLogin->fkprofessional != null){
            $user = $userLogin->pd_tprofessional()->first();
            $user['state']= 3;
            /*$city = $tmp->pd_tcity()->first();
            $user['city'] = $city->city_name;
            $user['country'] = $city->pd_tregion()->first()->pd_tcountry()->first()->country_name;*/
        }

        $data = [
            'token'=> $token,
            'user' => $user
        ];
        // todo bien devuelve el token
        return response()->json($data);
    }

    public function profile(Request $request){
        $token = JWTAuth::parseToken()->authenticate();

        $userLogin = PdTlogin::where('id', $token->id)->first();
        $user = [];
        if ($userLogin->fkpatient != null) {
            $tmp = $userLogin->pd_tpatient()->first();
            $city = $userLogin->pd_tpatient()->first()->pd_tcity()->first();
            $region = $city->pd_tregion()->first();
            $country = $region->pd_tcountry()->select('pkcountry as id', 'country_name as name')->first();

            $user['username'] = $tmp->patient_username;
            $user['image'] = $tmp->patient_image;
            $user['address'] = $tmp->patient_address;
            $user['pain_years'] = $tmp->patient_painyears;
            $user['pain_level'] = $tmp->fkpainlevel;
            $user['pain_type'] = $tmp->fkpaintype;
            $user['location'] = [
                'city' => ['id'=>$city->pkcity, 'name'=> $city->city_name],
                'region' => ['id'=>$region->pkregion, 'name'=> $region->region_name],
                'country' => $country
            ];
            $user['painkillers'] = PdTpatientPainkiller::
            join('pd_tpainkillers', 'pd_tpainkillers.pkpainkiller', '=', 'pd_tpatient_painkiller.fkpainkiller')
            ->select('pd_tpainkillers.pkpainkiller as id', 'pd_tpainkillers.painkiller_name as name')
            ->where('pd_tpatient_painkiller.fkpatient', $tmp->pkpatient)->get();
            $user['email'] = $userLogin->email;
            $user['usage_painkiller'] = $tmp->patient_usagepainkiller;
            $user['other_painkiller'] = $tmp->patient_otherpainkillers;
            $user['story'] = $tmp->patient_painstory;
            $user['public'] = $tmp->patient_painstorypublic;
            $user['membership'] = $tmp->fkmembership;
            $user['membership_date'] = $tmp->patient_membershipvalidity;
	    $user['preference'] = PdTpreference::leftJoin('pd_tpatient_preference', function($q) use ($tmp){ $q->on('pd_tpatient_preference.fkpreference', '=', 'pd_tpreference.pkpreference')->where('pd_tpatient_preference.fkpatient', '=', $tmp->pkpatient); })->select('pkpreference', 'preference_name', 'patientpreference_selected')->get();
        }
        if ($userLogin->fkprofessional != null) {
            $tmp = $userLogin->pd_tprofessional()->first();
            $city = $userLogin->pd_tprofessional()->first()->pd_tcity()->first();
            $region = $city->pd_tregion()->first();
            $country = $region->pd_tcountry()->select('pkcountry as id', 'country_name as name')->first();
            $user['username'] = $tmp->professional_username;
            $user['name'] = $tmp->professional_name;
            $user['image'] = $tmp->professional_image;
            $user['address'] = $tmp->professional_address;
            $user['phone'] = $tmp->professional_phone;
            $user['website'] = $tmp->professional_website;
            $user['profession'] = $tmp->fkprofession;
            $user['about'] = $tmp->professional_about;
            $user['approach'] = $tmp->professional_approach;
            $user['stories'] = $tmp->professional_stories;
            $user['location'] = [
                'city' => ['id'=>$city->pkcity, 'name'=> $city->city_name],
                'region' => ['id'=>$region->pkregion, 'name'=> $region->region_name],
                'country' => $country,
                'latitude' => $tmp->professional_latitude,
                'longitude' => $tmp->professional_longitude
            ];
            $user['email'] = $userLogin->email;
            $user['membership'] = $tmp->fkmembership;
            $user['membership_date'] = $tmp->patient_membershipvalidity;
            $user['qualifications'] = PdTprofessionalQualifcation::
            join('pd_tqualifications', 'pd_tqualifications.pkqualification', '=', 'pd_tprofessional_qualifcations.fkqualification')
            ->select('pd_tqualifications.pkqualification as id', 'pd_tqualifications.qualification_name as name')
            ->where('pd_tprofessional_qualifcations.fkprofessional', $tmp->pkprofessional)->get();
            $user['specialities'] = PdTprofessionalSpeciality::
            join('pd_tspecialities', 'pd_tspecialities.pkspecialty', '=', 'pd_tprofessional_specialities.fkspecialty')
            ->select('pd_tspecialities.pkspecialty as id', 'pd_tspecialities.specialty_name as name')
            ->where('pd_tprofessional_specialities.fkprofessional', $tmp->pkprofessional)->get();

	    $user['preference'] = PdTpreference::leftJoin('pd_tprofessional_preference', function($q) use ($tmp){ $q->on('pd_tprofessional_preference.fkpreference', '=', 'pd_tpreference.pkpreference')->where('pd_tprofessional_preference.fkprofessional', '=', $tmp->pkprofessional); })->select('pkpreference', 'preference_name', 'patientpreference_selected')->get();




        }
        return $user;
    }

    public function getProfessionals(){
        $items = [];
        $token = JWTAuth::parseToken()->authenticate();

        return $items;

        if($token){
            $user = [];
            $userLogin = PdTlogin::where('id', $token->id)->first();
            $distance = env("DISTANTE");
            $isProfessional = $userLogin->fkprofessional != null ? true : false;
            if($isProfessional){ 
                $tmp = $userLogin->pd_tprofessional()->first();
                $user = ['lat'=> (double)$tmp->professional_latitude, 'lng'=> (double)$tmp->professional_longitude];
            }else {
                $tmp = $userLogin->pd_tpatient()->first();
                $user = ['lat'=> (double)$tmp->patient_latitude, 'lng'=> (double)$tmp->patient_longitude];
            }
            $box = $this->getBoundaries($user['lat'], $user['lng'], $distance);
            $query = 'SELECT *, ( 6371 * ACOS( 
                                     COS( RADIANS(' . $user['lat'] . ') ) 
                                     * COS(RADIANS( professional_latitude ) ) 
                                     * COS(RADIANS( professional_longitude ) 
                                     - RADIANS(' . $user['lng'] . ') ) 
                                     + SIN( RADIANS(' . $user['lat'] . ') ) 
                                     * SIN(RADIANS( professional_latitude ) ) 
                                    )
                       ) AS distance 
             FROM pd_tprofessional 
             WHERE (professional_latitude BETWEEN ' . $box['min_lat']. ' AND ' . $box['max_lat'] . ')
             AND (professional_longitude BETWEEN ' . $box['min_lng']. ' AND ' . $box['max_lng']. ')
             '. ($isProfessional ? ' AND pkprofessional != ' . $token->fkprofessional : '') . '
             HAVING distance < ' . $distance . '
             ORDER BY distance ASC';
             $data = DB::select(DB::raw($query));
        }else
            $data = PdTprofessional::all();

        foreach ($data as $d) {
            $user['username'] = $d->professional_username;
            $user['name'] = $d->professional_name;
            $user['image'] = $d->professional_image;
            if($token){
                $query = "select * from pd_tprofessions where pkprofession = " . $d->fkprofession;
                $user['profession'] = DB::select(DB::raw($query))[0];
            }else
                $user['profession'] = $d->pd_tprofession()->first();
            $user['about'] = $d->professional_about;
            $user['distance'] = number_format($d->distance, 1, '.', ',');

            //GeoCode
            /*$city = $d->pd_tcity()->first();
            $region = $city->pd_tregion()->first();
            $country = $region->pd_tcountry()->select('pkcountry as id', 'country_name as name')->first();
            $position = Geocode::make()->address($d->professional_address . ',' . $city->city_name . ',' . $region->region_name .',' . $country->name);*/
            $user['position'] = ['lat'=> (double)$d->professional_latitude, 'long'=> (double)$d->professional_longitude];
            array_push($items, $user);
        }
        return $items;
    }

}
