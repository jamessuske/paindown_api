<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Lib\Util;

use App\LbTlogin;
use App\LbTtradesman;
use App\LbTtradesman_login;
use App\LbTsubtypes;
use App\LbTaccreditation;
use App\LbTtradesman_trades;
use App\LbTtradesman_accreditation;
use App\LbTfar_radius;
use App\LbTbusiness_type;
use App\Models\PdTlogin;
use App\Models\PdTpatient;
use App\Models\PdTprofessional;
use App\Models\PdTprofessionalSpeciality;
use App\Models\PdTprofessionalQualifcation;
use Jcf\Geocode\Geocode;


class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function professional(Request $request)
    {
		
		$data = $request->all();
		

		$array = [
			'professional_name' => $data["professional_name"],
			'professional_username' => $data["professional_username"],
			'email' => $data["email"],
			'password' => $data["password"],
			'fkcountry' => $data["fkcountry"],
			'fkregion' => $data["fkregion"],
			'fkcity' => $data["fkcity"],
			'professional_address' => $data["professional_address"],
			'professional_phone' => $data["professional_phone"],
			'professional_website' => $data["professional_website"],
			'professional_about' => $data["professional_about"],
			'professional_approach' => $data["professional_approach"],
			'professional_stories' => $data["professional_stories"],
			'fkprofession' => $data["fkprofession"],
			'fkqualifications' => $data["fkqualifications"],
			'fkspecialities' => $data["fkspecialities"]
		];


		$validator = \Validator::make($array, [
			'professional_name' => 'required',
			'professional_username' => 'required',
			'email'  => 'required|email',
			'password' => 'required|min:8',
			'professional_address' => 'required',
			'professional_phone' => 'required'
		]);
		
		if ($validator->fails()){
            return [
                'status' => false,
                'message' => 'the request could not be processed',
                'errors'  => $validator->errors()
            ];
        }
		
		//Create Patient
		$professional = new PdTprofessional;
		$professional->professional_name = $data["professional_name"];
		$professional->professional_username = $data["professional_username"];
		$professional->professional_address = $data["professional_address"];
		$professional->professional_phone = $data["professional_phone"];
		$professional->fkcountry = $data["fkcountry"];
		$professional->fkregion = $data["fkregion"];
		$professional->fkcity = $data["fkcity"];
		$professional->professional_about = $data["professional_about"];
		$professional->professional_approach = $data["professional_approach"];
		$professional->professional_stories = $data["professional_stories"];
		$professional->professional_website = $data["professional_website"];
		$professional->professional_image = $data["professional_image"];
		$professional->fkprofession = $data["fkprofession"];
		$professional->fkmembership = 3;
		$response = Geocode::make()->address($professional->professional_address);
		if ($response) {

			$professional->professional_latitude = $response->latitude();
			$professional->professional_longitude = $response->longitude();

		}
		else
		{
		
			$prepAddr = str_replace(' ','+',$professional->professional_address . ',' .  $request["city_name"]);
                	$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address=' . $prepAddr . '&sensor=false&&key=AIzaSyBn58_p7Z-kdIRleiC8Hug_lnMSq4sn7Pw');
                	$output= json_decode($geocode);
                	$professional->professional_latitude = $output->results[0]->geometry->location->lat;
                	$professional->professional_longitude = $output->results[0]->geometry->location->lng;

		}

		if($professional->save())
		{

			foreach($array["fkqualifications"] as $q)
			{


				$qualification = new PdTprofessionalQualifcation;

				$qualification->fkqualification = $q["id"];
				
				$qualification->fkprofessional = $professional->pkprofessional;

				$qualification->save();


			}

			foreach($array["fkspecialities"] as $s) {
				
				$speciality = new PdTprofessionalSpeciality;

				$speciality->fkspecialty = $s["id"];

				$speciality->fkprofessional = $professional->pkprofessional;

				$speciality->save();

			}

			$login = new PdTlogin;
			$login->email = $data["email"];
			$login->password = bcrypt($data["password"]);
			$login->fkprofessional = $professional->pkprofessional;
			$login->active = 1;
			$login->fkrol = 2;
			
			if($login->save()) {
				
				return Util::getSuccess();
				
			}
			else
			{
				return Util::getErrors();
			}
			
		}
		else
		{
            		return Util::getErrors();
        	}
		
		//Create Login
		
		
        /*$array = [
            'first_name' =>$request->detail['first_name'],
            'last_name' =>$request->detail['last_name'],
            'phone' => $request->detail['phone'],
            'username' => $request->detail['username'],
            'city' => $request->location['value']['id'],
            'zip_code' => $request->location['postcode'],
            'email' =>$request->detail['email'],
            'password' => $request->detail['password'],
        ];

        $validator = \Validator::make($array, [
            'first_name' => 'required|min:3',
            'last_name'     => 'required|min:3',
            'email'  => 'required|email',
            'phone'  => 'required|numeric',
            'city' =>  'required|numeric',
            'zip_code' => 'required',
            'username' => 'required',
            'password' => 'required|min:3',
        ]);

        if ($validator->fails()){
            return [
                'status' => false,
                'message' => 'the request could not be processed',
                'errors'  => $validator->errors()
            ];
        }

        $tradesman = new LbTtradesman;
        $tradesman->tradesman_firstname = $request->detail['first_name'];
        $tradesman->tradesman_lastname  = $request->detail['last_name'];
        $tradesman->tradesman_phone = $request->detail['phone'];
        $tradesman->tradesman_username  = $request->detail['username'];
        $tradesman->fkcity  = $request->location['value']['id'];
        $tradesman->fkpostcode  = $request->location['postcode'];

        if ($tradesman->save()) {
            $login = new LbTlogin;
            $login->email = $request->detail['email'];
            $login->password = bcrypt($request->detail['password']);
            $login->fktradesman = $tradesman->pktradesman;
            $login->save();
            return [ 
                'status'=> Util::getSuccess(), 
                'id'=> $login->fktradesman, 
                'username' => $tradesman->tradesman_username,
                'postcode' => $tradesman->fkpostcode,
                'city' => $request->location['value']['value'],
                'name' => $tradesman->tradesman_firstname.' '.$tradesman->tradesman_lastname,
            ];
        }else{
            return Util::getErrors();
        }*/

    }
    public function store(Request $request)
    {
		
		$data = $request->all();
		
		$array = [
			'patient_username' => $data["patient_username"],
			'email' => $data["email"],
			'password' => $data["password"],
			'fkcountry' => $data["fkcountry"],
			'fkregion' => $data["fkregion"],
			'fkcity' => $data["fkcity"],
			'patient_address' => $data["patient_address"],
			'fkpaintype' => $data["fkpaintype"],
			'fkpainlevel' => $data["fkpainlevel"],
			'patient_painyears' => $data["patient_painyears"],
			'patient_usagepainkiller' => $data["patient_usagepainkiller"],
			'patient_otherpainkillers' => $data["patient_otherpainkillers"],
			'patient_painstorypublic' => $data["patient_painstorypublic"],
			'patient_painstory' => $data["patient_painstory"]
		];
		
		$validator = \Validator::make($array, [
			'patient_username' => 'required',
			'email'  => 'required|email',
			'password' => 'required|min:8',
			'patient_address' => 'required'
		]);
		
		if ($validator->fails()){
            return [
                'status' => false,
                'message' => 'the request could not be processed',
                'errors'  => $validator->errors()
            ];
        }
		
		
		//Create Patient
		$patient = new PdTpatient;
		$patient->patient_username = $data["patient_username"];
		$patient->patient_address = $data["patient_address"];
		$patient->patient_painyears = $data["patient_painyears"];
		$patient->fkpainlevel = $data["fkpainlevel"];
		$patient->fkpaintype = $data["fkpaintype"];
		$patient->fkcountry = $data["fkcountry"];
		$patient->fkregion = $data["fkregion"];
		$patient->fkcity = $data["fkcity"];
		$patient->patient_usagepainkiller = $data["patient_usagepainkiller"];
		$patient->patient_otherpainkillers = $data["patient_otherpainkillers"];
		$patient->patient_painstory = $data["patient_painstory"];
		$patient->patient_painstorypublic = $data["patient_painstorypublic"];
		$patient->patient_image = $data["patient_image"];
		$patient->fkmembership = 3;
		$response = Geocode::make()->address($patient->patient_address);
		if ($response) {

			$patient->patient_latitude = $response->latitude();
			$patient->patient_longitude = $response->longitude();

		}
                else
                {

                        $prepAddr = str_replace(' ','+',$patient->patient_address . ',' .  $request["city_name"]);
                        $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address=' . $prepAddr . '&sensor=false&&key=AIzaSyBn58_p7Z-kdIRleiC8Hug_lnMSq4sn7Pw');
                        $output= json_decode($geocode);
                        $patient->patient_latitude = $output->results[0]->geometry->location->lat;
                        $patient->patient_longitude = $output->results[0]->geometry->location->lng;

                }
		
		//echo json_encode($patient);
		
		if($patient->save())
		{
			$login = new PdTlogin;
			$login->email = $data["email"];
			$login->password = bcrypt($data["password"]);
			$login->fkpatient = $patient->pkpatient;
			$login->active = 1;
			$login->fkrol = 2;
			
			if($login->save()) {
				
				return Util::getSuccess();
				
			}
			else
			{
				return Util::getErrors();
			}
			
		}
		else
		{
            return Util::getErrors();
        }
		
		//Create Login
		
		
        /*$array = [
            'first_name' =>$request->detail['first_name'],
            'last_name' =>$request->detail['last_name'],
            'phone' => $request->detail['phone'],
            'username' => $request->detail['username'],
            'city' => $request->location['value']['id'],
            'zip_code' => $request->location['postcode'],
            'email' =>$request->detail['email'],
            'password' => $request->detail['password'],
        ];

        $validator = \Validator::make($array, [
            'first_name' => 'required|min:3',
            'last_name'     => 'required|min:3',
            'email'  => 'required|email',
            'phone'  => 'required|numeric',
            'city' =>  'required|numeric',
            'zip_code' => 'required',
            'username' => 'required',
            'password' => 'required|min:3',
        ]);

        if ($validator->fails()){
            return [
                'status' => false,
                'message' => 'the request could not be processed',
                'errors'  => $validator->errors()
            ];
        }

        $tradesman = new LbTtradesman;
        $tradesman->tradesman_firstname = $request->detail['first_name'];
        $tradesman->tradesman_lastname  = $request->detail['last_name'];
        $tradesman->tradesman_phone = $request->detail['phone'];
        $tradesman->tradesman_username  = $request->detail['username'];
        $tradesman->fkcity  = $request->location['value']['id'];
        $tradesman->fkpostcode  = $request->location['postcode'];

        if ($tradesman->save()) {
            $login = new LbTlogin;
            $login->email = $request->detail['email'];
            $login->password = bcrypt($request->detail['password']);
            $login->fktradesman = $tradesman->pktradesman;
            $login->save();
            return [ 
                'status'=> Util::getSuccess(), 
                'id'=> $login->fktradesman, 
                'username' => $tradesman->tradesman_username,
                'postcode' => $tradesman->fkpostcode,
                'city' => $request->location['value']['value'],
                'name' => $tradesman->tradesman_firstname.' '.$tradesman->tradesman_lastname,
            ];
        }else{
            return Util::getErrors();
        }*/

    }
    public function typeregister(){
        $subtypes = LbTsubtypes::select('pksubtypes as id', 'subtypes_name as name')->get();
        $accreditation = LbTaccreditation::select('pkaccreditation as id', 'accreditation_name as name')->get();
        $radius = LbTfar_radius::select('pkfar_radius as id', 'far_radius_name as name')->get();
        $business = LbTbusiness_type::select('pkbusiness_type as id', 'businesstype_name as name')->get();

        $data = [
            'subtype' => $subtypes,
            'accreditation'  =>$accreditation,
            'radius' => $radius,
            'business_type' => $business,
        ];

        return $data;
    }

    public function registerTrades(Request $request){
        
        $array = [
            'fktradesman' => $request->user['id'],
            'fksubtypes' => $request->subtype['id'],
            'fkaccreditation' =>$request->accreditation['id'],
        ];

        $validator = \Validator::make($array, [
            'fktradesman' => 'required|numeric',
            'fksubtypes' => 'required|numeric',
            'fkaccreditation' => 'required|numeric',
        ]);

        if ($validator->fails()){
            return [
                'status' => false,
                'message' => 'the request could not be processed',
                'errors'  => $validator->errors()
            ];
        }

        $tradesman = $request->user['id'];
        $tradesman_trades = new LbTtradesman_trades;
        $tradesman_trades->fksubtypes = $request->subtype['id'];
        $tradesman_trades->fktradesman = $tradesman;

        if ($tradesman_trades->save()) {
            $LbTtradesman_accreditation = new LbTtradesman_accreditation;
            $LbTtradesman_accreditation->fkaccreditation = $request->accreditation['id'];
            $LbTtradesman_accreditation->fktradesman = $tradesman;
            $LbTtradesman_accreditation->save();
            return Util::getSuccess();
        }else{
            return Util::getErrors();
        }
    }

    public function businessTrades(Request $request){
        $array = [
            'fktradesman' => $request->user['id'],
            'tradesman_description' => $request->business['introduction'],
            'tradesman_working' => $request->business['working'],
            'tradesman_qualification' =>$request->business['qualifications'],
        ];

        $validator = \Validator::make($array, [
            'fktradesman' => 'required|numeric',
            'tradesman_description' => 'required|min:5',
            'tradesman_working' => 'required|min:5',
            'tradesman_qualification' => 'required|min:5',
        ]);

        if ($validator->fails()){
            return [
                'status' => false,
                'message' => 'the request could not be processed',
                'errors'  => $validator->errors()
            ];
        }

        $tradesman = LbTtradesman::Find($request->user['id']);
        
        if ($tradesman) {
            $tradesman->tradesman_description = $request->business['introduction'];
            $tradesman->tradesman_working = $request->business['working'];
            $tradesman->tradesman_qualification = $request->business['qualifications'];

            if ($tradesman->save()) {
                 return Util::getSuccess();
            }else{
                return Util::getErrors();
            }
        }
    }

    public function companyTrades(Request $request){

        $array = [
            'fktradesman' => $request->user['id'],
            'fkbusiness_type' => $request->business_type['id'],
            'tradesman_company' => $request->company['name'],
            'tradesman_address1' => $request->company['address_one'],
            'tradesman_address2' => $request->company['address_two'],
            'fkfar_radius' => $request->radius['id'],
        ];

        $validator = \Validator::make($array, [
            'fktradesman' => 'required|numeric',
            'fkbusiness_type' => 'required|numeric',
            'tradesman_company' => 'required|min:3',
            'tradesman_address1' => 'required|min:3',
            'tradesman_address2' => 'required|min:3',
            'fkfar_radius' => 'required|numeric',
        ]);

        if ($validator->fails()){
            return [
                'status' => false,
                'message' => 'the request could not be processed',
                'errors'  => $validator->errors()
            ];
        }

        $tradesman = LbTtradesman::FindOrFail($request->user['id']);

        if ($tradesman) {
            $tradesman->fkbusiness_type = $request->business_type['id'];
            $tradesman->tradesman_company = $request->company['name'];
            $tradesman->tradesman_address1 = $request->company['address_one'];
            $tradesman->tradesman_address2 = $request->company['address_two'];
            $tradesman->fkfar_radius = $request->radius['id'];

            if ($tradesman->save()) {
                 return Util::getSuccess();
            }else{
                return Util::getErrors();
            }
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
