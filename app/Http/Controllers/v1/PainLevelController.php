<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\PdTpainlevel;

class PainLevelController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PdTpainlevel::select('pkpainlevel as id', 'painlevel_name as name')->get();
    }

    public function admin()
    {
		$data = PdTpainlevel::select('pkpainlevel as id', 'painlevel_name as name')->get();
		return view('painlevel', compact('data'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('painlevel-add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
		PdTpainlevel::create($data);
		
        return redirect()->to('/admin/painlevel');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = PdTpainlevel::find($id);
		return view('painlevel-delete', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = PdTpainlevel::find($id);
		return view('painlevel-edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = PdTpainlevel::find($id);
		$data->update($request->all());
		
        return redirect()->to('/admin/painlevel');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = PdTpainlevel::find($id);
		$data->delete();
		
        return redirect()->to('/admin/painlevel');
    }
}
