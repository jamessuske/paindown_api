<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\PdTprofessional;
use App\Models\PdTspeciality;
use Geocode;
use JWTAuth;
use App\Models\PdTlogin;
use App\Models\PdTprofession;
use App\Models\PdRole;

class ProfessionalsController extends Controller
{
   
	public function professionalsMap($location)
        {
                $locationArray = [];
                /*$response = Geocode::make()->address($location);
                if ($response) {
                        $locationArray['latitude'] = $response->latitude();
                        $locationArray['longitude'] = $response->longitude();
		}*/
		$prepAddr = str_replace(' ','+',$location);
		$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address=' . $prepAddr . '&sensor=false&&key=AIzaSyBn58_p7Z-kdIRleiC8Hug_lnMSq4sn7Pw');
        	$output= json_decode($geocode);
		$locationArray['latitude'] = $output->results[0]->geometry->location->lat;
        	$locationArray['longitude'] = $output->results[0]->geometry->location->lng;
                return $locationArray;
        }
 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$items = [];
		$user = [];
		$data = PdTprofessional::orderBy('professional_rating', 'DESC')->get();
	foreach ($data as $d) {
		$city = $d->pd_tcity()->first();
            	$region = $city->pd_tregion()->first();
            	$country = $region->pd_tcountry()->select('pkcountry as id', 'country_name as name')->first();
            $user['username'] = $d->professional_username;
            $user['name'] = $d->professional_name;
            $user['image'] = $d->professional_image;
            $query = "select * from pd_tprofessions where pkprofession = " . $d->fkprofession;
            $user['profession'] = DB::select(DB::raw($query))[0];
            $user['profession_name'] = $user['profession']->profession_name;
            $query = "select * from pd_tspecialities e inner join pd_tprofessional_specialities pe ";
            $query.= "ON e.pkspecialty = pe.fkspecialty where fkprofessional = " . $d->pkprofessional;
            $sp = DB::select(DB::raw($query));
            $user['speciality'] = "";
            if($sp){
                foreach ($sp as $key => $s)
                    $user["speciality"] .= $s->specialty_name . (count($sp) - 1 == $key ? '' : ', ');
            }
            $user['about'] = $d->professional_about;
            $user['distance'] = isset($d->distance) ? number_format($d->distance, 1, '.', ',') : null;
            $user['rating'] = $d->professional_rating;
            $user['position'] = ['lat'=> (double)$d->professional_latitude, 'long'=> (double)$d->professional_longitude];
	    $user['location'] = [
                'city' => ['id'=>$city->pkcity, 'name'=> $city->city_name],
                'region' => ['id'=>$region->pkregion, 'name'=> $region->region_name],
                'country' => $country
            ];
            array_push($items, $user);
        }
        return $items;
    }
	
	public function admin()
	{
		$items = [];
		$user = [];
		$data = PdTprofessional::orderBy('professional_rating', 'DESC')->get();
		foreach ($data as $d) {
			$user['pkprofessional'] = $d->pkprofessional;
			$user['email'] = PdTLogin::where('fkprofessional', $d->pkprofessional)->first()->email;
            $user['username'] = $d->professional_username;
            $user['name'] = $d->professional_name;
            $user['image'] = $d->professional_image;
            $query = "select * from pd_tprofessions where pkprofession = " . $d->fkprofession;
            $user['profession'] = DB::select(DB::raw($query))[0];
            $user['profession_name'] = $user['profession']->profession_name;
            $query = "select * from pd_tspecialities e inner join pd_tprofessional_specialities pe ";
            $query.= "ON e.pkspecialty = pe.fkspecialty where fkprofessional = " . $d->pkprofessional;
            $sp = DB::select(DB::raw($query));
            $user['speciality'] = "";
            if($sp){
                foreach ($sp as $key => $s)
                    $user["speciality"] .= $s->specialty_name . (count($sp) - 1 == $key ? '' : ', ');
            }
            $user['about'] = $d->professional_about;
            $user['distance'] = isset($d->distance) ? number_format($d->distance, 1, '.', ',') : null;
            $user['rating'] = $d->professional_rating;
            $user['position'] = ['lat'=> (double)$d->professional_latitude, 'long'=> (double)$d->professional_longitude];
            array_push($items, $user);
        }
		return view('professionals', compact('items'));
	}
	
    public function indexMap($long, $lat)
    {
		$items = [];
		$user = [];
		$data = PdTprofessional::select(DB::raw('professional_username, professional_name, professional_image, fkprofession, pkprofessional, professional_rating, professional_about, professional_latitude, professional_longitude, (6371 * acos(cos(radians(' . $lat . ')) * cos(radians(professional_latitude)) * cos (radians(professional_longitude) - radians(' . $long . ')) + sin(radians(' . $lat . ')) * sin(radians(professional_latitude)))) as distance'))->orderBy('professional_rating', 'DESC')->get();
		foreach ($data as $d) {
            $user['username'] = $d->professional_username;
            $user['name'] = $d->professional_name;
            $user['image'] = $d->professional_image;
            $query = "select * from pd_tprofessions where pkprofession = " . $d->fkprofession;
            $user['profession'] = DB::select(DB::raw($query))[0];
            $user['profession_name'] = $user['profession']->profession_name;
            $query = "select * from pd_tspecialities e inner join pd_tprofessional_specialities pe ";
            $query.= "ON e.pkspecialty = pe.fkspecialty where fkprofessional = " . $d->pkprofessional;
            $sp = DB::select(DB::raw($query));
            $user['speciality'] = "";
            if($sp){
                foreach ($sp as $key => $s)
                    $user["speciality"] .= $s->specialty_name . (count($sp) - 1 == $key ? '' : ', ');
            }
            $user['about'] = $d->professional_about;
            $user['distance'] = isset($d->distance) ? number_format($d->distance, 0) : null;
            $user['rating'] = $d->professional_rating;
            $user['position'] = ['lat'=> (double)$d->professional_latitude, 'long'=> (double)$d->professional_longitude];
            array_push($items, $user);
        }
        return $items;
    }


    public function filter($pros, $spec, $location, $order, $disMin, $disMax, $long, $lat)
    {
		$items = [];
		$user = [];



		$data = PdTprofessional::select(DB::raw("professional_username, professional_name, professional_image, fkprofession, pkprofessional, professional_about, professional_rating, professional_latitude, professional_longitude, (6371 * acos(cos(radians(" . $lat . ")) * cos(radians(professional_latitude)) * cos(radians(professional_longitude) - radians(" . $long . ")) + sin(radians(" . $lat . ")) * sin(radians(professional_latitude)))) as distance"));
		
		if($spec != "0")
		{
			$data = $data->join('pd_tprofessional_specialities', 'pd_tprofessional.pkprofessional', '=', 'pd_tprofessional_specialities.fkprofessional')->where('pd_tprofessional_specialities.fkspecialty', '=', $spec);
		}
		if($pros != "0")
		{
			$data = $data->where('fkprofession', '=', $pros);
		}
		if($order != "0")
		{
			$data = $data->whereBetween('professional_rating', array(number_format($order, 1), (number_format($order, 1) + .9)));
		}
		if($location != "0")
		{
			$data = $data->havingRaw('distance < ' . $location);
		}
		if($disMin != 0 || $disMax != 0) 
		{
			$data = $data->whereRaw('(6371 * acos(cos(radians(43.3317767)) * cos(radians(professional_latitude)) * cos(radians(professional_longitude) - radians(-79.79917189999999)) + sin(radians(43.3317767)) * sin(radians(professional_latitude)))) BETWEEN ' . $disMin . ' AND ' .  $disMax);
		}

		$data = $data->orderBy('professional_rating', 'DESC')->get();



		foreach ($data as $d) {
            $user['username'] = $d->professional_username;
            $user['name'] = $d->professional_name;
            $user['image'] = $d->professional_image;
            $query = "select * from pd_tprofessions where pkprofession = " . $d->fkprofession;
            $user['profession'] = DB::select(DB::raw($query))[0];
            $user['profession_name'] = $user['profession']->profession_name;
            $query = "select * from pd_tspecialities e inner join pd_tprofessional_specialities pe ";
            $query.= "ON e.pkspecialty = pe.fkspecialty where fkprofessional = " . $d->pkprofessional;
            $sp = DB::select(DB::raw($query));
            $user['speciality'] = "";
            if($sp){
                foreach ($sp as $key => $s)
                    $user["speciality"] .= $s->specialty_name . (count($sp) - 1 == $key ? '' : ', ');
            }
            $user['about'] = $d->professional_about;
	    $user['distance'] = isset($d->distance) ? number_format($d->distance, 0) : null;
            $user['rating'] = $d->professional_rating;
            $user['position'] = ['lat'=> (double)$d->professional_latitude, 'long'=> (double)$d->professional_longitude];
            array_push($items, $user);
        }
        return $items;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$profession = PdTprofession::select('pkprofession as id', 'profession_name as text')->get();
		$speciality = PdTspeciality::select('pkspecialty as id', 'specialty_name as text')->get();
		$roles = PdRole::select('pkrol as id', 'rol_fullname as text')->get();
        return view('professionals-add', compact('profession', 'speciality', 'roles'));
    }
	
	public function add(Request $request)
	{
		$data = $request->all();
		PdTprofessional::create($data);
		
        return redirect()->to('/admin/patients');
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		
		
		$items = [];
		$user = [];
		$data = PdTprofessional::all();
		foreach ($data as $d) {
            $user['username'] = $d->professional_username;
            $user['name'] = $d->professional_name;
            $user['image'] = $d->professional_image;
            $query = "select * from pd_tprofessions where pkprofession = " . $d->fkprofession;
            $user['profession'] = DB::select(DB::raw($query))[0];
            $user['profession_name'] = $user['profession']->profession_name;
            $query = "select * from pd_tspecialities e inner join pd_tprofessional_specialities pe ";
            $query.= "ON e.pkspecialty = pe.fkspecialty where fkprofessional = " . $d->pkprofessional;
            $sp = DB::select(DB::raw($query));
            $user['speciality'] = "";
            if($sp){
                foreach ($sp as $key => $s)
                    $user["speciality"] .= $s->specialty_name . (count($sp) - 1 == $key ? '' : ', ');
            }
            $user['about'] = $d->professional_about;
            $user['distance'] = isset($d->distance) ? number_format($d->distance, 1, '.', ',') : null;
            $user['rating'] = $d->professional_rating;
            $user['position'] = ['lat'=> (double)$d->professional_latitude, 'long'=> (double)$d->professional_longitude];
            array_push($items, $user);
        }
        return $items;
		
        /*$items = [];
        try{
            $token = JWTAuth::parseToken()->authenticate();
        }catch(\Exception $e){
            $token = null;
        }
        
        if($token){
            $user = [];
            $userLogin = PdTlogin::where('id', $token->id)->first();
            $distance = env("DISTANTE");
            $isProfessional = $userLogin->fkprofessional != null ? true : false;
            if($isProfessional){ 
                $tmp = $userLogin->pd_tprofessional()->first();
                $user = ['lat'=> (double)$tmp->professional_latitude, 'lng'=> (double)$tmp->professional_longitude];
            }else {
                $tmp = $userLogin->pd_tpatient()->first();
                $user = ['lat'=> (double)$tmp->patient_latitude, 'lng'=> (double)$tmp->patient_longitude];
            }
            $box = $this->getBoundaries($user['lat'], $user['lng'], $distance);
            $query = 'SELECT *, ( 6371 * ACOS( 
                                     COS( RADIANS(' . $user['lat'] . ') ) 
                                     * COS(RADIANS( professional_latitude ) ) 
                                     * COS(RADIANS( professional_longitude ) 
                                     - RADIANS(' . $user['lng'] . ') ) 
                                     + SIN( RADIANS(' . $user['lat'] . ') ) 
                                     * SIN(RADIANS( professional_latitude ) ) 
                                    )
                       ) AS distance 
             FROM pd_tprofessional p ';
             if($request->sp != "all"){
                $query .= " inner join pd_tprofessional_specialities ps on p.pkprofessional = ps.fkprofessional";
                $query .= " where ps.fkspecialty = " . $request->sp;
             }
             if($request->prof != "all")
                $query .= ($request->sp != "all" ? " and " : " where") . " p.fkprofession = " . $request->prof;
             $query .= ($request->sp != "all" || $request->prof != "all" ? " and " : " where");
             $query .= '(professional_latitude BETWEEN ' . $box['min_lat']. ' AND ' . $box['max_lat'] . ')
             AND (professional_longitude BETWEEN ' . $box['min_lng']. ' AND ' . $box['max_lng']. ')
             '. ($isProfessional ? ' AND pkprofessional != ' . $token->fkprofessional : '') . '
             HAVING distance BETWEEN ' . $request->min . ' and ' . $request->max . '
             ORDER BY distance ASC';
             $data = DB::select(DB::raw($query));
        }else{
            $query = 'SELECT * FROM pd_tprofessional p ';
             if($request->sp != "all"){
                $query .= " inner join pd_tprofessional_specialities ps on p.pkprofessional = ps.fkprofessional";
                $query .= " where ps.fkspecialty = " . $request->sp;
             }
             if($request->prof != "all")
                $query .= ($request->sp != "all" ? " and " : " where") . " p.fkprofession = " . $request->prof;
             $data = DB::select(DB::raw($query));
        }

        foreach ($data as $d) {
            $user['username'] = $d->professional_username;
            $user['name'] = $d->professional_name;
            $user['image'] = $d->professional_image;
            $query = "select * from pd_tprofessions where pkprofession = " . $d->fkprofession;
            $user['profession'] = DB::select(DB::raw($query))[0];
            $user['profession_name'] = $user['profession']->profession_name;
            $query = "select * from pd_tspecialities e inner join pd_tprofessional_specialities pe ";
            $query.= "ON e.pkspecialty = pe.fkspecialty where fkprofessional = " . $d->pkprofessional;
            $sp = DB::select(DB::raw($query));
            $user['speciality'] = "";
            if($sp){
                foreach ($sp as $key => $s)
                    $user["speciality"] .= $s->specialty_name . (count($sp) - 1 == $key ? '' : ', ');
            }
            $user['about'] = $d->professional_about;
            $user['distance'] = isset($d->distance) ? number_format($d->distance, 1, '.', ',') : null;
            $user['rating'] = $d->professional_rating;
            $user['position'] = ['lat'=> (double)$d->professional_latitude, 'long'=> (double)$d->professional_longitude];
            array_push($items, $user);
        }
        return $items;*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($username)
    {
		
		$items = [];
		$user = [];
		$data = PdTprofessional::where('professional_username', $username)->get();
		foreach ($data as $d) {
            $user['username'] = $d->professional_username;
            $user['name'] = $d->professional_name;
            $user['image'] = $d->professional_image;
            $query = "select * from pd_tprofessions where pkprofession = " . $d->fkprofession;
            $user['profession'] = DB::select(DB::raw($query))[0];
            $user['profession_name'] = $user['profession']->profession_name;
            $query = "select * from pd_tspecialities e inner join pd_tprofessional_specialities pe ";
            $query.= "ON e.pkspecialty = pe.fkspecialty where fkprofessional = " . $d->pkprofessional;
            $sp = DB::select(DB::raw($query));
            $user['speciality'] = "";
            if($sp){
                foreach ($sp as $key => $s)
                    $user["speciality"] .= $s->specialty_name . (count($sp) - 1 == $key ? '' : ', ');
            }
            $user['about'] = $d->professional_about;
            $user['distance'] = isset($d->distance) ? number_format($d->distance, 1, '.', ',') : null;
            $user['rating'] = $d->professional_rating;
            $user['position'] = ['lat'=> (double)$d->professional_latitude, 'long'=> (double)$d->professional_longitude];
            array_push($items, $user);
        }
        return $items;
    }
	
	public function user($id)
    {
		
		$items = [];
		$user = [];
		$data = PdTprofessional::where('pkprofessional', $id)->get();
		foreach ($data as $d) {
			$user['pkprofessional'] = $d->pkprofessional;
            $user['username'] = $d->professional_username;
            $user['name'] = $d->professional_name;
            $user['image'] = $d->professional_image;
            $query = "select * from pd_tprofessions where pkprofession = " . $d->fkprofession;
            $user['profession'] = DB::select(DB::raw($query))[0];
            $user['profession_name'] = $user['profession']->profession_name;
            $query = "select * from pd_tspecialities e inner join pd_tprofessional_specialities pe ";
            $query.= "ON e.pkspecialty = pe.fkspecialty where fkprofessional = " . $d->pkprofessional;
            $sp = DB::select(DB::raw($query));
            $user['speciality'] = "";
            if($sp){
                foreach ($sp as $key => $s)
                    $user["speciality"] .= $s->specialty_name . (count($sp) - 1 == $key ? '' : ', ');
            }
            $user['about'] = $d->professional_about;
            $user['distance'] = isset($d->distance) ? number_format($d->distance, 1, '.', ',') : null;
            $user['rating'] = $d->professional_rating;
            $user['position'] = ['lat'=> (double)$d->professional_latitude, 'long'=> (double)$d->professional_longitude];
            array_push($items, $user);
        }
        return view('professionals-delete', compact('items'));
    }
	
	public function professions($professions)
    {
		
		$items = [];
		$user = [];
		$data = PdTprofessional::where('fkprofession', $professions)->get();
		foreach ($data as $d) {
            $user['username'] = $d->professional_username;
            $user['name'] = $d->professional_name;
            $user['image'] = $d->professional_image;
            $query = "select * from pd_tprofessions where pkprofession = " . $d->fkprofession;
            $user['profession'] = DB::select(DB::raw($query))[0];
            $user['profession_name'] = $user['profession']->profession_name;
            $query = "select * from pd_tspecialities e inner join pd_tprofessional_specialities pe ";
            $query.= "ON e.pkspecialty = pe.fkspecialty where fkprofessional = " . $d->pkprofessional;
            $sp = DB::select(DB::raw($query));
            $user['speciality'] = "";
            if($sp){
                foreach ($sp as $key => $s)
                    $user["speciality"] .= $s->specialty_name . (count($sp) - 1 == $key ? '' : ', ');
            }
            $user['about'] = $d->professional_about;
            $user['distance'] = isset($d->distance) ? number_format($d->distance, 1, '.', ',') : null;
            $user['rating'] = $d->professional_rating;
            $user['position'] = ['lat'=> (double)$d->professional_latitude, 'long'=> (double)$d->professional_longitude];
            array_push($items, $user);
        }
        return $items;
    }
	
	public function specialities($specialities)
    {
		$items = [];
		$user = [];
		$newQuery = "select * from pd_tspecialities e inner join pd_tprofessional_specialities pe ON e.pkspecialty = pe.fkspecialty inner join pd_tprofessional pro on pro.pkprofessional = fkprofessional WHERE fkspecialty = " . $specialities;
		$data = DB::select(DB::raw($newQuery));
		foreach ($data as $d) {
            $user['username'] = $d->professional_username;
            $user['name'] = $d->professional_name;
            $user['image'] = $d->professional_image;
            $query = "select * from pd_tprofessions where pkprofession = " . $d->fkprofession;
            $user['profession'] = DB::select(DB::raw($query))[0];
            $user['profession_name'] = $user['profession']->profession_name;
            $query = "select * from pd_tspecialities e inner join pd_tprofessional_specialities pe ";
            $query.= "ON e.pkspecialty = pe.fkspecialty where fkprofessional = " . $d->pkprofessional;
            $sp = DB::select(DB::raw($query));
            $user['speciality'] = "";
            if($sp){
                foreach ($sp as $key => $s)
                    $user["speciality"] .= $s->specialty_name . (count($sp) - 1 == $key ? '' : ', ');
            }
            $user['about'] = $d->professional_about;
            $user['distance'] = isset($d->distance) ? number_format($d->distance, 1, '.', ',') : null;
            $user['rating'] = $d->professional_rating;
            $user['position'] = ['lat'=> (double)$d->professional_latitude, 'long'=> (double)$d->professional_longitude];
            array_push($items, $user);
        }
        return $items;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$profession = PdTprofession::select('pkprofession as id', 'profession_name as text')->get();
		$speciality = PdTspeciality::select('pkspecialty as id', 'specialty_name as text')->get();
		$roles = PdRole::select('pkrol as id', 'rol_fullname as text')->get();
		$items = [];
		$user = [];
		$data = PdTprofessional::where('pkprofessional', $id)->get();
		foreach ($data as $d) {
			$user['pkprofessional'] = $d->pkprofessional;
            $user['username'] = $d->professional_username;
            $user['name'] = $d->professional_name;
            $user['image'] = $d->professional_image;
            $query = "select * from pd_tprofessions where pkprofession = " . $d->fkprofession;
            $user['profession'] = DB::select(DB::raw($query))[0];
            $user['profession_name'] = $d->fkprofession;
            $query = "select * from pd_tspecialities e inner join pd_tprofessional_specialities pe ";
            $query.= "ON e.pkspecialty = pe.fkspecialty where fkprofessional = " . $d->pkprofessional;
            $sp = DB::select(DB::raw($query));
            $user['speciality'] = [];
            if($sp){
                foreach ($sp as $key => $s)
                    //$user["speciality"] .= $s->pkspecialty . (count($sp) - 1 == $key ? '' : ', ');
					array_push($user["speciality"], $s->pkspecialty);
            }
            $user['about'] = $d->professional_about;
            $user['distance'] = isset($d->distance) ? number_format($d->distance, 1, '.', ',') : null;
            $user['rating'] = $d->professional_rating;
            $user['position'] = ['lat'=> (double)$d->professional_latitude, 'long'=> (double)$d->professional_longitude];
            array_push($items, $user);
        }
        return view('professionals-edit', compact('items', 'profession', 'speciality', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$data = PdTprofessional::find($id);
		$data->update($request->all());
		
        return redirect()->to('/admin/professionals');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = PdTprofessional::find($id);
	$data->delete();
	$user = PdTlogin::where('fkprofessional', $id);
	$user->delete();
	//print_r($user);
		
        return redirect()->to('/admin/professionals');
    }

    function getBoundaries($lat, $lng, $distance = 1, $earthRadius = 6371)
    {
        $return = array();
         
        // Los angulos para cada dirección
        $cardinalCoords = array('north' => '0',
                                'south' => '180',
                                'east' => '90',
                                'west' => '270');
        $rLat = deg2rad($lat);
        $rLng = deg2rad($lng);
        $rAngDist = $distance/$earthRadius;
        foreach ($cardinalCoords as $name => $angle)
        {
            $rAngle = deg2rad($angle);
            $rLatB = asin(sin($rLat) * cos($rAngDist) + cos($rLat) * sin($rAngDist) * cos($rAngle));
            $rLonB = $rLng + atan2(sin($rAngle) * sin($rAngDist) * cos($rLat), cos($rAngDist) - sin($rLat) * sin($rLatB));
             $return[$name] = array('lat' => (float) rad2deg($rLatB), 
                                    'lng' => (float) rad2deg($rLonB));
        }
        return array('min_lat'  => $return['south']['lat'],
                     'max_lat' => $return['north']['lat'],
                     'min_lng' => $return['west']['lng'],
                     'max_lng' => $return['east']['lng']);
    }
}
