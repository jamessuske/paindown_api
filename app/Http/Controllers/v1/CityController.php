<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\LbTcity;
use App\LbTcountry;
use App\LbTregion;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($text)
    {

        $query= " select 
            city.pkcity as id, 

            CONCAT(LOWER(city.city_name), ', ', LOWER(region.region_name), ', ', LOWER(country.country_name) ) AS value 
            FROM lb_tcity city JOIN lb_tregion region ON city.fkregion =region.pkregion
            JOIN lb_tcountry country ON region.fkcountry = country.pkcountry 
            WHERE (city.city_name like '%".$text."%' OR region.region_name like '%".$text."%' 
            OR CONCAT(LOWER(city.city_name), ' ', LOWER(region.region_name)) like '%".$text."%') 
            AND country.country_active = true
            LIMIT 10
        ";
        $city = DB::select(DB::raw($query));

        $data =[
            'city' => $city
        ];
        return $city;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
