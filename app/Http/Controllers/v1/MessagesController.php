<?php

namespace App\Http\Controllers\v1;

use App\Models\PdTpatient;
use App\Models\PdTprofessional;
use App\Models\PdTlogin;
use JWTAuth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class MessagesController extends Controller
{
    /**
     * Show all of the message threads to the user.
     *
     * @return mixed
     */
    public function index()
    {

	$token = JWTAuth::parseToken()->authenticate();

        // All threads, ignore deleted/archived participants
        //$threads = Thread::getAllLatest()->get();

        // All threads that user is participating in
        //$threads = Thread::forUser($token->id)->latest('updated_at')->get();

        // All threads that user is participating in, with new messages
        $threads = Thread::forUserWithNewMessages($token->id)->latest('updated_at')->get();
	$counter = 0;
	foreach($threads as $thread)
	{
		$message = Message::where('thread_id', $thread->id)->first();
		$user = PdTlogin::where('id', $message->user_id)->first();
        	$data = PdTpatient::where('pkpatient', $user->fkpatient)->get()->first();
		if($data) {
			$threads[$counter]["username"] = $data->patient_username;
		}
		else
		{
			$data = PdTprofessional::where('pkprofessional', $user->fkprofessional)->get();
			$threads[$counter]["username"] = $data->professional_username;
		}
		$counter++;
	}

	return $threads;
    }


    public function getBySubject($subject) {

	$threads = Thread::getBySubject($subject);
	$counter = 0;
	foreach($threads as $thread)
	{
		$message = Message::where('thread_id', $thread->id)->first();
		$user = PdTlogin::where('id', $message->user_id)->first();
        	$data = PdTpatient::where('pkpatient', $user->fkpatient)->get()->first();
		if($data) {
			$threads[$counter]["username"] = $data->patient_username;
		}
		else
		{
			$data = PdTprofessional::where('pkprofessional', $user->fkprofessional)->get();
			$threads[$counter]["username"] = $data->professional_username;
		}
		$counter++;
	}

	return $threads;

    }

    public function sent()
    {
	$token = JWTAuth::parseToken()->authenticate();
        $threads = Thread::forUser($token->id)->latest('updated_at')->get();
	$counter = 0;
	foreach($threads as $thread)
	{
		$message = Message::where('thread_id', $thread->id)->first();
		$user = PdTlogin::where('id', $message->user_id)->first();
        	$data = PdTpatient::where('pkpatient', $user->fkpatient)->get()->first();
		if($data) {
			$threads[$counter]["username"] = $data->patient_username;
		}
		else
		{
			$data = PdTprofessional::where('pkprofessional', $user->fkprofessional)->get();
			$threads[$counter]["username"] = $data->professional_username;
		}
		$counter++;
	}
	return $threads;
    }

    public function deleted()
    {
	$token = JWTAuth::parseToken()->authenticate();
        $threads = Thread::forDeleted($token->id)->latest('updated_at')->get();
	$counter = 0;
	foreach($threads as $thread)
	{
		$message = Message::where('thread_id', $thread->id)->first();
		$user = PdTlogin::where('id', $message->user_id)->first();
        	$data = PdTpatient::where('pkpatient', $user->fkpatient)->get()->first();
		if($data) {
			$threads[$counter]["username"] = $data->patient_username;
		}
		else
		{
			$data = PdTprofessional::where('pkprofessional', $user->fkprofessional)->get();
			$threads[$counter]["username"] = $data->professional_username;
		}
		$counter++;
	}
	return $threads;
    }

    /**
     * Shows a message thread.
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {


	$messageArray = [];
	$token = JWTAuth::parseToken()->authenticate();
	
	try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');

	    $error['error_message'] = 'The thread with ID: ' . $id . ' was not found.';

            return $error;
        }

        // show current user in list if not a current participant
        // $users = User::whereNotIn('id', $thread->participantsUserIds())->get();

        // don't show the current user in list
        //$userId = Auth::id();
        //$users = User::whereNotIn('id', $thread->participantsUserIds($token->id))->get();

	$messages = Message::where('thread_id', $thread->id)->get();
	
	$thread->markAsRead($token->id);

	foreach($messages as $message)
	{
		$user = PdTlogin::where('id', $message->user_id)->first();
        	$data = PdTpatient::where('pkpatient', $user->fkpatient)->get()->first();
		if($data) {
			$message["username"] = $data->patient_username;
		}
		else
		{
			$data = PdTprofessional::where('pkprofessional', $user->fkprofessional)->get();
			$message["username"] = $data->professional_username;
		}
	}

	$messageArray["messages"] = $messages;

	$messageArray["thread"] = $thread;

	return json_encode($messageArray);
    }

    /**
     * Creates a new message thread.
     *
     * @return mixed
     */
    public function compose($username)
    {
	$items = [];
	$user = [];
        $data = PdTpatient::where('patient_username', $username)->get()->first();
	if($data){
        	$data = PdTpatient::where('patient_username', $username)->get();
		foreach ($data as $d) {
			$user['username'] = $d->patient_username;
			$userLogin = PdTlogin::where('fkpatient', $d->pkpatient)->first();
			$user['id'] = $userLogin->id;
			array_push($items, $user);
		}
	}
	else
	{
		$data = PdTprofessional::where('professional_username', $username)->get();
		foreach ($data as $d) {
			$user['username'] = $d->professional_username;
			$userLogin = PdTlogin::where('fkprofessional', $d->pkprofessional)->first();
			$user['id'] = $userLogin->id;
			array_push($items, $user);
		}
	}

	return $items;

    }
    

    /**
     * Stores a new message thread.
     *
     * @return mixed
     */
    public function store(Request $request)
    {

	$token = JWTAuth::parseToken()->authenticate();

	$data = PdTpatient::where('patient_username', $request['recipients'])->get()->first();

	if($data)
	{
		$userLogin = PdTlogin::where('fkpatient', $data->pkpatient)->first();
	}
	else
	{
		$data = PdTprofessional::where('professional_username', $request['recipients'])->get()->first();
		$userLogin = PdTlogin::where('fkprofessional', $data->pkprofessional)->first();
	}

        $thread = Thread::create([
            'subject' => $request['subject'],
        ]);

        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => $token->id,
            'body' => $request['message'],
        ]);

        // Sender
        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => $token->id,
            'last_read' => new Carbon,
        ]);

        // Recipients
	$thread->addParticipant($userLogin->id);
	
	return json_encode($request);

    }

    /**
     * Adds a new message to a current thread.
     *
     * @param $id
     * @return mixed
     */
    public function update(Request $request)
    {
        try {
            $thread = Thread::findOrFail($request['id']);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');

        }

        $thread->activateAllParticipants();
	
	$token = JWTAuth::parseToken()->authenticate();

        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => $token->id,
            'body' => $request['message'],
        ]);

        // Add replier as a participant
        $participant = Participant::firstOrCreate([
            'thread_id' => $thread->id,
            'user_id' => $token->id,
        ]);
        $participant->last_read = new Carbon;
        $participant->save();

        // Recipients
        //$thread->addParticipant($input['recipients']);

	$object["message"] = "done";

	return json_encode($object);
    }

    public function delete(Request $input)
    {
	
	$token = JWTAuth::parseToken()->authenticate();
	
	try {
            $thread = Thread::findOrFail($input['id']);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');

        }

	$thread->removeParticipant($token->id);

	return json_encode('done');

    }
}
