<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\PdTpatient;
use JWTAuth;
use App\Models\PdTlogin;
use Jcf\Geocode\Geocode;
use App\Models\PdTpaintype;
use App\Models\PdTpainlevel;
use App\Models\PdTpainkiller;
use App\Models\PdRole;
use App\Models\PdPatientPainkiller;
use App\Models\PdTmembership;
use App\Models\PdTcountry;
use App\Models\PdTregion;
use App\Models\PdTcity;

class PatientsController extends Controller
{


    	public function patientsMap($location)
   	{
		$locationArray = [];
		/*$response = Geocode::make()->address($location);
                if ($response) {
                        $locationArray['latitude'] = $response->latitude();
                        $locationArray['longitude'] = $response->longitude();
		}*/
		$prepAddr = str_replace(' ','+',$location);
                $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address=' . $prepAddr . '&sensor=false&&key=AIzaSyBn58_p7Z-kdIRleiC8Hug_lnMSq4sn7Pw');
                $output= json_decode($geocode);
                $locationArray['latitude'] = $output->results[0]->geometry->location->lat;
                $locationArray['longitude'] = $output->results[0]->geometry->location->lng;
                return $locationArray;
 	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = [];
        $data = PdTpatient::all();
	$data = PdTpatient::orderBy('patient_painyears', 'DESC')->get();
        foreach ($data as $tmp) {
	    $city = $tmp->pd_tcity()->first();
	    $region = $city->pd_tregion()->first();
	    $country = $region->pd_tcountry()->select('pkcountry as id', 'country_name as name')->first();
	    $user['location'] = [
                'city' => ['id'=>$city->pkcity, 'name'=> $city->city_name],
                'region' => ['id'=>$region->pkregion, 'name'=> $region->region_name],
                'country' => $country
            ];
            $user['username'] = $tmp->patient_username;
            $user['image'] = $tmp->patient_image;
            $user['pain_years'] = $tmp->patient_painyears;
            $user['pain_level'] = $tmp->pd_tpainlevel()->first()->painlevel_name;
            $user['pain_type'] = $tmp->pd_tpaintype()->first()->paintype_name;
            $user['usage_painkiller'] = $tmp->patient_usagepainkiller;
            $user['aa'] = $tmp->patient_painstorypublic;
	    if($tmp->patient_painstorypublic == 1)
	    {
                $user['story'] = $tmp->patient_painstory;
	    }
	    else
	    {
		    $user['story'] = "";
	    }
	    $user['position'] = ['lat'=> (double)$tmp->patient_latitude, 'long'=> (double)$tmp->patient_longitude];
            array_push($items, $user);
        }
        return $items;
    }
	
	public function admin()
	{
		$items = [];
        $data = PdTpatient::all();
	$data = PdTpatient::orderBy('patient_painyears', 'DESC')->get();
	foreach ($data as $tmp) {
	    $user['email'] = PdTLogin::where('fkpatient', $tmp->pkpatient)->first()->email;
	    $user['pkpatient'] = $tmp->pkpatient;
            $user['username'] = $tmp->patient_username;
            $user['image'] = $tmp->patient_image;
            $user['pain_years'] = $tmp->patient_painyears;
            $user['pain_level'] = $tmp->pd_tpainlevel()->first()->painlevel_name;
            $user['pain_type'] = $tmp->pd_tpaintype()->first()->paintype_name;
            $user['usage_painkiller'] = $tmp->patient_usagepainkiller;
	    $user['aa'] = $tmp->patient_painstorypublic;
            $user['story'] = $tmp->patient_painstory;
	    $user['position'] = ['lat'=> (double)$tmp->patient_latitude, 'long'=> (double)$tmp->patient_longitude];
            array_push($items, $user);
        }
		return view('patients', compact('items'));
	}

    public function indexMap($long, $lat)
    {
        $items = [];
	$data = PdTpatient::select(DB::raw("patient_username, patient_image, patient_painyears, patient_usagepainkiller, patient_painstorypublic, patient_painstory, patient_latitude, patient_longitude, fkpainlevel, fkpaintype, (6371 * acos(cos(radians(" . $lat . ")) * cos(radians(patient_latitude)) * cos(radians(patient_longitude) - radians(" . $long . ")) + sin(radians(" . $lat . ")) * sin(radians(patient_latitude)))) as distance"))->orderBy('patient_painyears', 'DESC')->get();
        foreach ($data as $tmp) {
            $user['username'] = $tmp->patient_username;
            $user['image'] = $tmp->patient_image;
            $user['pain_years'] = $tmp->patient_painyears;
            $user['pain_level'] = $tmp->pd_tpainlevel()->first()->painlevel_name;
            $user['pain_type'] = $tmp->pd_tpaintype()->first()->paintype_name;
            $user['usage_painkiller'] = $tmp->patient_usagepainkiller;
	    $user['distance'] = isset($tmp->distance) ? number_format($tmp->distance, 0) : null;
            $user['aa'] = $tmp->patient_painstorypublic;
	    if($tmp->patient_painstorypublic == 1)
	    {
                $user['story'] = $tmp->patient_painstory;
	    }
	    else
	    {
		    $user['story'] = "";
	    }
	    $user['position'] = ['lat'=> (double)$tmp->patient_latitude, 'long'=> (double)$tmp->patient_longitude];
            array_push($items, $user);
        }
        return $items;
    }

    public function show($username)
    {
        $items = [];
	$data = PdTpatient::where('patient_username', $username)->get();
        foreach ($data as $tmp) {
            $user['username'] = $tmp->patient_username;
            $user['image'] = $tmp->patient_image;
            $user['pain_years'] = $tmp->patient_painyears;
            $user['pain_level'] = $tmp->pd_tpainlevel()->first()->painlevel_name;
            $user['pain_type'] = $tmp->pd_tpaintype()->first()->paintype_name;
            $user['usage_painkiller'] = $tmp->patient_usagepainkiller;
            $user['aa'] = $tmp->patient_painstorypublic;
	    if($tmp->patient_painstorypublic == 1)
	    {
                $user['story'] = $tmp->patient_painstory;
	    }
	    else
	    {
		    $user['story'] = "";
	    }
	    $user['position'] = ['lat'=> (double)$tmp->patient_latitude, 'long'=> (double)$tmp->patient_longitude];
            array_push($items, $user);
        }
        return $items;
    }
	
	public function user($id)
    {
        $items = [];
		$data = PdTpatient::where('pkpatient', $id)->get();
        foreach ($data as $tmp) {
			$user['pkpatient'] = $tmp->pkpatient;
            $user['username'] = $tmp->patient_username;
            $user['image'] = $tmp->patient_image;
            $user['pain_years'] = $tmp->patient_painyears;
            $user['pain_level'] = $tmp->pd_tpainlevel()->first()->painlevel_name;
            $user['pain_type'] = $tmp->pd_tpaintype()->first()->paintype_name;
            $user['usage_painkiller'] = $tmp->patient_usagepainkiller;
            $user['aa'] = $tmp->patient_painstorypublic;
			if($tmp->patient_painstorypublic == 1)
			{
					$user['story'] = $tmp->patient_painstory;
			}
			else
			{
				$user['story'] = "";
			}
			$user['position'] = ['lat'=> (double)$tmp->patient_latitude, 'long'=> (double)$tmp->patient_longitude];
			array_push($items, $user);
        }
        return view('patients-delete', compact('items'));
    }

    public function filter($painType, $location, $order, $disMin, $disMax, $painMin, $painMax, $painKillers, $long, $lat)
    {
	    $items = [];
	    $data = PdTpatient::select(DB::raw("patient_username, patient_image, patient_painyears, painlevel_name, paintype_name, patient_usagepainkiller, patient_painstorypublic, patient_painstory, patient_latitude, patient_longitude, fkpaintype, (6371 * acos(cos(radians(" . $lat . ")) * cos(radians(patient_latitude)) * cos(radians(patient_longitude) - radians(" . $long . ")) + sin(radians(" . $lat . ")) * sin(radians(patient_latitude)))) as distance"));

	    $data = $data->join("pd_tpainlevel", "pd_tpainlevel.pkpainlevel", "=", "pd_tpatient.fkpainlevel");

	    $data = $data->join("pd_tpaintype", "pd_tpaintype.pkpaintype", "=", "pd_tpatient.fkpaintype");
	    
	    	if($painType != "0")
		{
			$data = $data->where('pkpaintype', '=', $painType);
		}
		if($painMin != 0 || $painMax != 0)
		{
			$data = $data->whereRaw('pd_tpatient.patient_painyears BETWEEN ' . $painMin . ' AND ' . $painMax);
		}
		if($order != "0")
		{
			
			$orderMin = "0";
			$orderMax = "0";

			if($order == "1"){
				$orderMin = "0";
				$orderMax = "1";
			}else if($order == "2"){
				$orderMin = "2";
				$orderMax = "3";
			}else if($order == "3"){
				$orderMin = "4";
				$orderMax = "5";
			}else{
				$orderMin = "5";
				$orderMax = "999999999999";
			}

			$data = $data->whereBetween('patient_painyears', array($orderMin, $orderMax));

		}

		if($location != "0")
		{
			$data = $data->havingRaw('distance < ' . $location);
		}

		if($disMin != 0 || $disMax != 0)
                {
                        $data = $data->whereRaw('(6371 * acos(cos(radians(43.3317767)) * cos(radians(patient_latitude)) * cos(radians(patient_longitude) - radians(-79.79917189999999)) + sin(radians(43.3317767)) * sin(radians(patient_latitude)))) BETWEEN ' . $disMin . ' AND ' .  $disMax);
                }
		if($painKillers == "Yes")
		{
			$data = $data->where('patient_usagepainkiller', '>', 0);
		}
		if($painKillers == "No")
		{
			$data = $data->where('patient_usagepainkiller', '=', 0);
		}

		$data = $data->orderBy('pd_tpatient.patient_painyears', 'DESC')->get();	

        foreach ($data as $tmp) {
            $user['username'] = $tmp->patient_username;
            $user['image'] = $tmp->patient_image;
            $user['pain_years'] = $tmp->patient_painyears;
            $user['pain_level'] = $tmp->painlevel_name;
            $user['pain_type'] = $tmp->paintype_name;
            $user['usage_painkiller'] = $tmp->patient_usagepainkiller;
            $user['aa'] = $tmp->patient_painstorypublic;
	    $user['distance'] = isset($tmp->distance) ? number_format($tmp->distance, 0) : null;
	    if($tmp->patient_painstorypublic == 1)
	    {
                $user['story'] = $tmp->patient_painstory;
	    }
	    else
	    {
		    $user['story'] = "";
	    }
	    $user['position'] = ['lat'=> (double)$tmp->patient_latitude, 'long'=> (double)$tmp->patient_longitude];

            array_push($items, $user);
        }
        return $items;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$painlevel = PdTpainlevel::select('pkpainlevel as id', 'painlevel_name as name')->get();
		$paintype = PdTpaintype::select('pkpaintype as id', 'paintype_name as name')->get();
		$painkiller = PdTpainkiller::select('pkpainkiller as id', 'painkiller_name as name')->get();
		$roles = PdRole::select('pkrol as id', 'rol_fullname as text')->get();
        	return view('patients-add', compact('painlevel', 'paintype', 'painkiller', 'roles'));
    }
	
	public function add(Request $request)
	{
		
		$data = $request->all();
		PdTpatient::create($data);
		
        return redirect()->to('/admin/patients');
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $items = [];
        try{
            $token = JWTAuth::parseToken()->authenticate();
        }catch(\Exception $e){
            $token = null;
        }
        
        if($token){
            $user = [];
            $userLogin = PdTlogin::where('id', $token->id)->first();
            $distance = env("DISTANTE");
            $isPatient = $userLogin->fkpatient != null ? true : false;
            if(!$isPatient){ 
                $tmp = $userLogin->pd_tprofessional()->first();
                $user = ['lat'=> (double)$tmp->professional_latitude, 'lng'=> (double)$tmp->professional_longitude];
            }else {
                $tmp = $userLogin->pd_tpatient()->first();
                $user = ['lat'=> (double)$tmp->patient_latitude, 'lng'=> (double)$tmp->patient_longitude];
            }
            $box = $this->getBoundaries($user['lat'], $user['lng'], $distance);
            $query = 'SELECT *, ( 6371 * ACOS( 
                                     COS( RADIANS(' . $user['lat'] . ') ) 
                                     * COS(RADIANS( patient_latitude ) ) 
                                     * COS(RADIANS( patient_longitude ) 
                                     - RADIANS(' . $user['lng'] . ') ) 
                                     + SIN( RADIANS(' . $user['lat'] . ') ) 
                                     * SIN(RADIANS( patient_latitude ) ) 
                                    )
                       ) AS distance 
             FROM pd_tpatient p ';
             if($request->pt != "all"){
                $query .= " where p.fkpaintype = " . $request->pt;
             }
             $query .= ($request->pt != "all" ? " and " : " where") .
                " p.patient_painyears BETWEEN " . $request->minp . " and " . $request->maxp . " ";
             $query .= ($request->pt != "all" ? " and " : " where ");
             $query .= '(patient_latitude BETWEEN ' . $box['min_lat']. ' AND ' . $box['max_lat'] . ')
             AND (patient_longitude BETWEEN ' . $box['min_lng']. ' AND ' . $box['max_lng']. ')
             '. ($isPatient ? ' AND pkpatient != ' . $token->fkpatient : '') . '
             HAVING distance BETWEEN ' . $request->min . ' and ' . $request->max . '
             ORDER BY distance ASC';
             $data = DB::select(DB::raw($query));
        }else{
            $query = 'SELECT * FROM pd_tpatient p ';
             if($request->pt != "all")
                 $query .= " where p.fkpaintype = " . $request->pt;
            $query .= ($request->pt != "all" ? " and " : " where") . 
                " p.patient_painyears BETWEEN " . $request->minp . " and " . $request->maxp . " ";
             $data = DB::select(DB::raw($query));
        }

        foreach ($data as $d) {
            $user['username'] = $d->patient_username;
            $user['image'] = $d->patient_image;
            $user['pain_years'] = $d->patient_painyears;
            $query = "select * from pd_tpaintype where pkpaintype = " . $d->fkpaintype;
            $user['pain_type'] = DB::select(DB::raw($query))[0];
            $user['pain_t'] = $user['pain_type']->paintype_name;
            $query = "select * from pd_tpainlevel where pkpainlevel = " . $d->fkpainlevel;
            $user['pain_level'] = DB::select(DB::raw($query))[0];
            $user['pain_l'] = $user['pain_level']->painlevel_name;
            $user['usage_painkiller'] = $d->patient_usagepainkiller;
            if($d->patient_painstorypublic == 1)
                $user['story'] = $d->patient_painstory;
            $user['position'] = ['lat'=> (double)$d->patient_latitude, 'long'=> (double)$d->patient_longitude];
            array_push($items, $user);
        }
        return $items;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$painlevel = PdTpainlevel::select('pkpainlevel as id', 'painlevel_name as name')->get();
		$paintype = PdTpaintype::select('pkpaintype as id', 'paintype_name as name')->get();
		$painkiller = PdTpainkiller::select('pkpainkiller as id', 'painkiller_name as name')->get();
		$roles = PdRole::select('pkrol as id', 'rol_fullname as text')->get();
		$memberships = PdTmembership::select('pkmembership as id', 'membership_name as text')->get();
		$items = [];
		$data = PdTpatient::where('pkpatient', $id)->get();
		foreach ($data as $tmp) {
			$user['email'] = PdTLogin::where('fkpatient', $tmp->pkpatient)->first()->email;
			$user['fkrol'] = PdTLogin::where('fkpatient', $tmp->pkpatient)->first()->fkrol;
			$user['pkpatient'] = $tmp->pkpatient;
            		$user['username'] = $tmp->patient_username;
            		$user['image'] = $tmp->patient_image;
			$user['patient_address'] = $tmp->patient_address;
                	$user['fkcity'] = $tmp->fkcity;
                	$user['fkregion'] = $tmp->fkregion;
                	$user['fkcountry'] = $tmp->fkcountry;
            		$user['pain_years'] = $tmp->patient_painyears;
            		$user['pain_level'] = $tmp->fkpainlevel;
            		$user['pain_type'] = $tmp->fkpaintype;
            		$user['usage_painkiller'] = $tmp->patient_usagepainkiller;
			$user['patient_otherpainkillers'] = $tmp->patient_otherpainkillers;
			$user['patient_painstorypublic'] = $tmp->patient_painstorypublic;
			$user['fkmembership'] = $tmp->fkmembership;
            		$user['aa'] = $tmp->patient_painstorypublic;
			if($tmp->patient_painstorypublic == 1)
			{
				$user['story'] = $tmp->patient_painstory;
			}
			else
			{
				$user['story'] = "";
			}
			$user['position'] = ['lat'=> (double)$tmp->patient_latitude, 'long'=> (double)$tmp->patient_longitude];
			array_push($items, $user);
        	}
		return view('patients-edit', compact('items', 'painlevel', 'paintype', 'painkiller', 'roles', 'memberships'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    print_r($request->usage_painkiller);

	    $userLogin = PdTlogin::where('fkpatient', $id)->first();
	    $userLogin->email = $request->email;
	    $userLogin->fkrol = $request->role;
	    $userLogin->save();
	    $user = $userLogin->pd_tpatient()->first();
            if($user){
		$user->patient_username = $request->username;
                $user->patient_address = $request->address;
                $user->fkpainlevel = $request->pain_level;
                $user->fkpaintype = $request->pain_type;
                $user->patient_painyears = $request->pain_years;
                $user->patient_painstory = $request->story;
                $user->patient_usagepainkiller = $request->usage_painkiller;
                $user->patient_otherpainkillers = $request->patient_otherpainkillers;
                $user->patient_painstorypublic = $request->patient_painstorypublic;
		$user->fkmembership = $request->membership;
                /*$response = Geocode::make()->address($request->location["city"]["name"] . ", " . $request->location["region"]["name"]);
                if ($response) {
                        $user->patient_latitude = $response->latitude();
                        $user->patient_longitude = $response->longitude();
		}*/

		$user->save();

	    }
		
        return redirect()->to('/admin/patients');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = PdTpatient::find($id);
	$data->delete();
	$user = PdTlogin::where('fkpatient', $id);
        $user->delete();	
        return redirect()->to('/admin/patients');
    }

    function getBoundaries($lat, $lng, $distance = 1, $earthRadius = 6371)
    {
        $return = array();
         
        // Los angulos para cada dirección
        $cardinalCoords = array('north' => '0',
                                'south' => '180',
                                'east' => '90',
                                'west' => '270');
        $rLat = deg2rad($lat);
        $rLng = deg2rad($lng);
        $rAngDist = $distance/$earthRadius;
        foreach ($cardinalCoords as $name => $angle)
        {
            $rAngle = deg2rad($angle);
            $rLatB = asin(sin($rLat) * cos($rAngDist) + cos($rLat) * sin($rAngDist) * cos($rAngle));
            $rLonB = $rLng + atan2(sin($rAngle) * sin($rAngDist) * cos($rLat), cos($rAngDist) - sin($rLat) * sin($rLatB));
             $return[$name] = array('lat' => (float) rad2deg($rLatB), 
                                    'lng' => (float) rad2deg($rLonB));
        }
        return array('min_lat'  => $return['south']['lat'],
                     'max_lat' => $return['north']['lat'],
                     'min_lng' => $return['west']['lng'],
                     'max_lng' => $return['east']['lng']);
    }
}
