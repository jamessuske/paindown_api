<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Lib\Util;
use JWTAuth;
use App\LbTtrades_type;
use App\LbTsubtypes;
use App\LbTpriority;
use App\LbTjob;
use App\LbTcity;
use App\LbTimage;
use App\LbTjobimage;



class PostJobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type = LbTtrades_type::select('pktrades as id', 'trades_name as name')->get();
        $subcategory = LbTsubtypes::select('pksubtypes as id', 'subtypes_name as name', 'fktrades_type as type')->get();
        $priority = LbTpriority::select('pkpriority as id', 'priority_name as name')->get();;
        $data =[
            'categories' => $type,
            'subcategories' => $subcategory,
            'priorities' => $priority
        ];

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $token = JWTAuth::parseToken()->authenticate();

        $array = [
            'job' =>[
                'title' =>$request->job['title'],
                'budget' =>$request->job['budget'],
                'description' =>$request->job['description'],
                'start_date' => $request->job['start_date_ar']['formatted'],
                'fkcity' => $request->location['id'],
                'fkpostcode' => $request->location['postcode'],
                'fksubtypes' => $request->classification['subtype']['id'],
                'fkpriority' => $request->fkpriority
            ]
        ];

        $validator = \Validator::make($array, [
            'job.title' => 'required|min:3',
            'job.budget'     => 'required|numeric',
            'job.description'  => 'required|min:5',
            'job.start_date'  => 'required',
            'job.fkcity' => 'required|numeric',
            'job.fkpostcode' => 'required|numeric',
            'job.fksubtypes' => 'required|numeric',
            'job.fkpriority' => 'required|numeric',
        ]);


        if ($validator->fails()){
            return [
                'status' => false,
                'message' => 'the request could not be processed',
                'errors'  => $validator->errors()
            ];
        }

        $job = new LbTjob;

        $job->job_title = $request->job['title'];
        $job->job_budget = $request->job['budget'];
        $job->job_description = $request->job['description'];
        $job->job_start_date = $request->job['start_date_ar']['formatted'];
        $job->job_status = 1;
        $job->fkcity = $request->location['id'];
        $job->fkpostcode = $request->location['postcode'];
        $job->fksubtypes = $request->classification['subtype']['id'];
        $job->fkpriority = $request->fkpriority;
        $job->fkcustomer = $token->fkcustomer;
        
        if ($job->save()) {
            if (isset($request->images)) {
               foreach ($request->images as $image) {
                   $img = new LbTimage;
                   $img->image_url = $image;
                   if ( $img->save() ) {
                       $jobImage = new LbTjobimage;
                       $jobImage->fkjob = $job->pkjob;
                       $jobImage->fkimage = $img->pkimage;
                       $jobImage->save();
                   }
               }
            }
            return Util::getSuccess();
        }else{
            return Util::getErrors();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function getMyJobs(Request $request)
    {
        $token = JWTAuth::parseToken()->authenticate();

        $query ="
            select 
                job.*, city.city_name, subtype.subtypes_name , type.pktrades, type.trades_name, type.trades_icon 
            from lb_tjob as job
            inner join 
                lb_tcity as city on job.fkcity = city.pkcity
            inner join 
                lb_tsubtypes as subtype on job.fksubtypes = subtype.pksubtypes
            inner join
                lb_ttrades_type as type on subtype.fktrades_type = type.pktrades
            where 
                fkcustomer = ".$token->fkcustomer."
            order by job.created_at desc";

        $jobs = DB::select(DB::raw($query));

        $data = [];
        foreach ($jobs as $job) {

            $query ="
                select 
                    image.image_url 
                from lb_timages as image 
                inner join lb_tjobimages as jobimage on image.pkimage = jobimage.fkimage 
                where fkjob =".$job->pkjob;

            $images = DB::select(DB::raw($query));

            $tmp = [
                'job'=>[
                    'id' => $job->pkjob,
                    'title'=> $job->job_title,
                    'budget'=> $job->job_budget,
                    'start_date'=> $job->job_start_date,
                    'created'=> $job->created_at,
                    'description'=> $job->job_description,
                    'images' => $images

                ],
                'location' =>[
                    'id'=> $job->fkcity,
                    'city'=> $job->city_name,
                    'postcode' => 123456
                ],
                'classification' =>[
                    'type' =>[
                        'id'=> $job->pktrades,
                        'name'=> $job->trades_name,
                        'icon'=> $job->trades_icon
                    ],
                    'subtype' =>[
                        'id'=> $job->fksubtypes,
                        'name'=> $job->subtypes_name
                    ]
                ]
            ];
            array_push($data, $tmp);
        }
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'job_title'      => 'required|min:3',
            'job_budget'     => 'required|numeric',
            'job_description'  => 'required|min:5',
            'job_start_date'  => 'required',
            'fkcity' => 'required|numeric',
            'fkpostcode' => 'required|numeric',
            'fksubtypes' => 'required|numeric',
            'fkpriority' => 'required|numeric',
        ];

        $validator = \Validator::make($request->all(), $rules);


        if ($validator->fails()) {
            return [
                'status' => false,
                'message' => 'the request could not be processed',
                'errors'  => $validator->errors()->all()
            ];
        }

        $job = LbTjob::FindOrFail($id);
        $job->job_title = $request->job_title;
        $job->job_budget = $request->job_budget;
        $job->job_description = $request->job_description;
        $job->job_start_date = $request->job_start_date;
        $job->fkcity = $request->fkcity;
        $job->fkpostcode = $request->fkpostcode;
        $job->fksubtypes = $request->fksubtypes;
        $job->fkpriority = $request->fkpriority;
        if ($job->save()) {
            return Util::getSuccess();
        }else{
            return Util::getErrors();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
