<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\PdTcountry;
use App\Models\PdTregion;
use App\Models\PdTcity;

class LocationController extends Controller
{
    
    public function getCountries($text = 'all'){
        $list = [];
        if($text == 'all')
            $list = PdTcountry::select('pkcountry as id', 'country_name as text')
            ->orderBy('text', 'asc')
            ->get();
        else
            $list = PdTcountry::select('pkcountry as id', 'country_name as text')
            ->where('text', 'like', '%' . $text . '%')
            ->where('fkcountry', $country)
            ->orderBy('text', 'asc')
            ->get();
        return $list;
    }

    public function getRegions($country){
        $list = PdTregion::select('pkregion as id', 'region_name as text')
            ->where('fkcountry', $country)
            ->orderBy('text', 'asc')
            ->get();
        return $list;
    }

    public function getCities($region){
        $list = PdTcity::select('pkcity as id', 'city_name as text')
            ->where('fkregion', $region)
            ->orderBy('text', 'asc')
            ->get();
        return $list;
    }
}
