@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Pain Types</h1>
@stop

@section('content')
	<form method="POST" action="/paindown_api/public/admin/paintype-edit/{{ $data->pkpaintype }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="form-group">
			<label for="paintype_name">Pain Type Name</label>
			<input type="text" name="paintype_name" id="paintype_name" class="form-control" value="{{$data->paintype_name}}" />
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-success">Update</button>
		</div>
	</form>
@stop
