@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Patients</h1>
@stop

@section('content')
	<form method="POST" action="/paindown_api/public/admin/patients-add">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="form-group">
			<label for="username">Username</label>
			<input type="text" name="username" id="username" class="form-control" />
		</div>
		<div class="form-group">
                        <label for="role">Role</label>
                        <select name="role" id="role" class="form-control">
                                <option id="0">-- Select Role --</option>
                                @foreach ($roles as $r)
                                        <option id="{{$r->id}}">{{$r->text}}</option>
                                @endforeach
                        </select>
                </div>
		<div class="form-group">
			<label for="pain_years">Pain Years</label>
			<input type="number" name="pain_years" id="pain_years" class="form-control" />
		</div>
		<div class="form-group">
			<label for="pain_level">Pain Level</label>
			<select name="pain_level" id="pain_level" class="form-control">
				<option id="0">-- Select Pain Level --</option>
				@foreach ($painlevel as $plevel)
					<option id="{{$plevel->id}}">{{$plevel->name}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="pain_type">Pain Type</label>
			<select name="pain_type" id="pain_type" class="form-control">
				<option id="0">-- Select Pain Type --</option>
				@foreach ($paintype as $ptype)
					<option id="{{$ptype->id}}">{{$ptype->name}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="usage_painkiller">Pain Killers</label>
			<select name="usage_painkiller" id="usage_painkiller" class="form-control">
				<option id="0">-- Select Pain Killer --</option>
				@foreach ($painkiller as $pkiller)
					<option id="{{$pkiller->id}}">{{$pkiller->name}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="story">Story</label>
			<textarea name="story" id="story" class="form-control" rows="10"></textarea>
		</div>
		<div class="form-group">
			<label for="latitude">Latitude</label>
			<input type="text" name="latitude" id="latitude" class="form-control" />
		</div>
		<div class="form-group">
			<label for="longitude">Longitude</label>
			<input type="text" name="longitude" id="longitude" class="form-control" />
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-success">Add</button>
		</div>
	</form>
@stop
