@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Qualification</h1>
@stop

@section('content')
	<form method="POST" action="/paindown_api/public/admin/qualifications-add">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="form-group">
			<label for="qualification_name">Qualification Name</label>
			<input type="text" name="qualification_name" id="qualification_name" class="form-control" />
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-success">Add</button>
		</div>
	</form>
@stop
