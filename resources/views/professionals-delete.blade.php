@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Professionals</h1>
@stop

@section('content')
    <div class="form-group">
		<label>Username: </label>
		<span>{{$items[0]['username']}}</span>
	</div>
	<div class="form-group">
		<label>Name: </label>
		<span>{{$items[0]['name']}}</span>
	</div>
	<div class="form-group">
		<label>Image: </label>
		<span><img src="{{ $items[0]['image'] }}" height="50" /></span>
	</div>
	<div class="form-group">
		<label>Profession: </label>
		<span>{{$items[0]['profession_name']}}</span>
	</div>
	<div class="form-group">
		<label>Speciality: </label>
		<span>{{$items[0]['speciality']}}</span>
	</div>
	<div class="form-group">
		<label>About: </label>
		<span>{{$items[0]['about']}}</span>
	</div>
	<div class="form-group">
		<label>Rating: </label>
		<span>{{$items[0]['rating']}}</span>
	</div>
	<div class="form-group">
		<label>Location: </label>
		<span>{{ $items[0]['position']['lat'] }}, {{$items[0]['position']['long']}}</span>
	</div>
	<form method="POST" action="/paindown_api/public/admin/professionals-delete/{{ $items[0]['pkprofessional'] }}">
		<div class="form-group">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<button type="submit" class="btn btn-success">Delete</button>
		</div>
	</form>
@stop
