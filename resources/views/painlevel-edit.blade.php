@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Pain Levels</h1>
@stop

@section('content')
	<form method="POST" action="/paindown_api/public/admin/painlevel-edit/{{ $data->pkpainlevel }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="form-group">
			<label for="painlevel_name">Pain Level Name</label>
			<input type="text" name="painlevel_name" id="painlevel_name" class="form-control" value="{{$data->painlevel_name}}" />
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-success">Update</button>
		</div>
	</form>
@stop
