@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Pain Levels</h1>
@stop

@section('content')
    <div class="form-group">
		<label>Pain Level Name: </label>
		<span>{{$data->painlevel_name}}</span>
	</div>
	<form method="POST" action="/paindown_api/public/admin/painlevel-delete/{{ $data->pkpainlevel }}">
		<div class="form-group">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<button type="submit" class="btn btn-success">Delete</button>
		</div>
	</form>
@stop
