@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Professions</h1>
@stop

@section('content')
	<form method="POST" action="/paindown_api/public/admin/professions-edit/{{ $data->pkprofession }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="form-group">
			<label for="profession_name">Profession Name</label>
			<input type="text" name="profession_name" id="profession_name" class="form-control" value="{{$data->profession_name}}" />
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-success">Update</button>
		</div>
	</form>
@stop
