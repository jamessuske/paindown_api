@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Pain Killers</h1>
@stop

@section('content')
	<form method="POST" action="/paindown_api/public/admin/painkillers-add">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="form-group">
			<label for="painkiller_name">Pain Killer Name</label>
			<input type="text" name="painkiller_name" id="painkiller_name" class="form-control" />
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-success">Add</button>
		</div>
	</form>
@stop
