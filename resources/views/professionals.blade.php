@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
	<h1 style="float: left;">Professionals</h1>
	<a href="professionals-add" class="btn btn-success" style="float: right;">Add New</a>
@stop

@section('content')
    <table class="table table-dark" style="clear: both;">
		<tr>
			<th>Username</th>
			<th>Email</th>
			<th>Name</th>
			<th>Image</th>
			<th>Profession</th>
			<th>Speciality</th>
			<th>Rating</th>
			<th></th>
			<th></th>
		</tr>
	@foreach ($items as $item)
		<tr>
			<td>{{ $item['username'] }}</td>
			<td>{{ $item['email'] }}</td>
			<td>{{ $item['name'] }}</td>
			<td><img src="{{ $item['image'] }}" height="50" /></td>
			<td>{{ $item['profession_name'] }}</td>
			<td>{{ $item['speciality'] }}</td>
			<td>{{ $item['rating'] }}</td>
			<td><a href="professionals-edit/{{$item['pkprofessional']}}" class="btn btn-default">Edit</a></td><td><a href="professionals-delete/{{$item['pkprofessional']}}" class="btn btn-default">Delete</a></td></tr>
	@endforeach
	</table>
@stop
