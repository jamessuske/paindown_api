@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Patients</h1>
@stop

@section('content')
    <div class="form-group">
		<label>Username: </label>
		<span>{{$items[0]['username']}}</span>
	</div>
	<div class="form-group">
		<label>Image: </label>
		<span><img src="{{ $items[0]['image'] }}" height="50" /></span>
	</div>
	<div class="form-group">
		<label>Pain Years: </label>
		<span>{{$items[0]['pain_years']}}</span>
	</div>
	<div class="form-group">
		<label>Pain Level: </label>
		<span>{{$items[0]['pain_level']}}</span>
	</div>
	<div class="form-group">
		<label>Pain Type: </label>
		<span>{{$items[0]['pain_type']}}</span>
	</div>
	<div class="form-group">
		<label>Pain Killers: </label>
		<span>{{$items[0]['usage_painkiller']}}</span>
	</div>
	<div class="form-group">
		<label>Story: </label>
		<span>{{$items[0]['story']}}</span>
	</div>
	<div class="form-group">
		<label>Location: </label>
		<span>{{ $items[0]['position']['lat'] }}, {{$items[0]['position']['long']}}</span>
	</div>
	<form method="POST" action="/paindown_api/public/admin/patients-delete/{{ $items[0]['pkpatient'] }}">
		<div class="form-group">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<button type="submit" class="btn btn-success">Delete</button>
		</div>
	</form>
@stop
