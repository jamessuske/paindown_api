@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Professionals</h1>
@stop

@section('content')
	<form method="POST" action="/paindown_api/public/admin/professionals-add">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="form-group">
			<label for="username">Username</label>
			<input type="text" name="username" id="username" class="form-control" />
		</div>
		<div class="form-group">
			<label for="name">Name</label>
			<input type="text" name="name" id="name" class="form-control" />
		</div>
		<!--<div class="form-group">
			<label for="image">Image</label>
			<input type="file" name="image" id="image" class="form-control" />
		</div>-->
		<div class="form-group">
			<label for="role">Role</label>
			<select name="role" id="role" class="form-control">
				<option id="0">-- Select Role --</option>
				@foreach ($roles as $r)
					<option id="{{$r->id}}">{{$r->text}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="profession_name">Profession</label>
			<select name="profession_name" id="profession_name" class="form-control">
				<option id="0">-- Select Profession --</option>
				@foreach ($profession as $p)
					<option id="{{$p->id}}">{{$p->text}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="speciality">Speciality</label>
			<select name="speciality" id="speciality" class="form-control" multiple>
				<option id="0">-- Select Specialities --</option>
				@foreach ($speciality as $s)
					<option id="{{$s->id}}">{{$s->text}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="about">About</label>
			<textarea name="about" id="about" class="form-control" rows="10"></textarea>
		</div>
		<div class="form-group">
			<label for="rating">Rating</label>
			<input type="number" step="0.5" max="5" min="0" name="rating" id="rating" class="form-control" />
		</div>
		<div class="form-group">
			<label for="latitude">Latitude</label>
			<input type="text" name="latitude" id="latitude" class="form-control" />
		</div>
		<div class="form-group">
			<label for="longitude">Longitude</label>
			<input type="text" name="longitude" id="longitude" class="form-control" />
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-success">Add</button>
		</div>
	</form>
@stop
