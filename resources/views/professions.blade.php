@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 style="float: left;">Professions</h1>
	<a href="professions-add" class="btn btn-success" style="float: right;">Add New</a>
@stop

@section('content')
    <table class="table table-dark" style="clear: both;">
		<tr>
			<th>Name</th>
			<th></th>
			<th></th>
		</tr>
	@foreach ($data as $item)
		<tr><td>{{ $item->text }}</td><td><a href="professions-edit/{{$item->id}}" class="btn btn-default">Edit</a></td><td><a href="professions-delete/{{$item->id}}" class="btn btn-default">Delete</a></td></tr>
	@endforeach
	</table>
@stop
