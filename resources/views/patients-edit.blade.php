@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Patients</h1>
@stop

@section('content')
	<form method="POST" action="/paindown_api/public/admin/patients-edit/{{ $items[0]['pkpatient'] }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="form-group">
			<label for="username">Username</label>
			<input type="text" name="username" id="username" class="form-control" value="{{$items[0]['username']}}" />
		</div>
		<div class="form-group">
			<label for="email">Email</label>
			<input type="text" name="email" id="email" class="form-control" value="{{$items[0]['email']}}" />
		</div>
		<div class="form-group">
			<label for="image">Image</label>
			<input type="file" name="image" id="image" class="form-control" />
			<input type="hidden" name="image" id="image" class="form-control" value="{{$items[0]['image']}}"  />
		</div>
		<div class="form-group">
			<label for="address">Address</label>
			<input type="text" name="address" id="address" class="form-control" value="{{$items[0]['patient_address']}}" />
		</div>
		<div class="form-group">
                        <label for="role">Role</label>
                        <select name="role" id="role" class="form-control">
                                <option id="0">-- Select Role --</option>
                                @foreach ($roles as $r)
					@if($r->id == $items[0]['fkrol'])
						<option value="{{$r->id}}" selected>{{$r->text}}</option>
					@else
						<option value="{{$r->id}}">{{$r->text}}</option>
					@endif
                                @endforeach
                        </select>
                </div>
		<div class="form-group">
                        <label for="membership">Membership</label>
                        <select name="membership" id="membership" class="form-control">
                                <option id="0">-- Select Membership --</option>
                                @foreach ($memberships as $m)
					@if($m->id == $items[0]['fkmembership'])
						<option value="{{$m->id}}" selected>{{$m->text}}</option>
					@else
						<option value="{{$m->id}}">{{$m->text}}</option>
					@endif
                                @endforeach
                        </select>
                </div>
		<div class="form-group">
			<label for="pain_years">Pain Years</label>
			<input type="number" name="pain_years" id="pain_years" class="form-control" value="{{$items[0]['pain_years']}}"  />
		</div>
		<div class="form-group">
			<label for="pain_level">Pain Level</label>
			<select name="pain_level" id="pain_level" class="form-control">
				<option id="0">-- Select Pain Level --</option>
				@foreach ($painlevel as $plevel)
					@if($plevel->id == $items[0]['pain_level'])
						<option value="{{$plevel->id}}" selected>{{$plevel->name}}</option>
					@else
						<option value="{{$plevel->id}}">{{$plevel->name}}</option>
					@endif
					
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="pain_type">Pain Type</label>
			<select name="pain_type" id="pain_type" class="form-control">
				<option id="0">-- Select Pain Type --</option>
				@foreach ($paintype as $ptype)
					@if($ptype->id == $items[0]['pain_type'])
						<option value="{{$ptype->id}}" selected>{{$ptype->name}}</option>
					@else
						<option value="{{$ptype->id}}">{{$ptype->name}}</option>
					@endif
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="usage_painkiller">Pain Killers</label>
			<select name="usage_painkiller" id="usage_painkiller" class="form-control">
				<option id="0">-- Select Pain Killer --</option>
				@foreach ($painkiller as $pkiller)
					@if($pkiller->id == $items[0]['usage_painkiller'])
						<option value="{{$pkiller->id}}" selected="selected">{{$pkiller->name}}</option>
					@else
						<option value="{{$pkiller->id}}">{{$pkiller->name}}</option>
					@endif
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="patient_otherpainkillers">Other Painkillers</label>
			<input type="text" name="patient_otherpainkillers" id="patient_otherpainkillers" class="form-control" value="{{$items[0]['patient_otherpainkillers']}}" />
		</div>
		<div class="form-group">
			<label for="patient_painstorypublic">Pain Story Public</label>
			<select name="patient_painstorypublic" id="patient_painstorypublic" class="form-control">
				@if($items[0]['patient_painstorypublic'] == 0)
				{
					<option value="0" selected="selected">No</option>
				}
				@else
				{
					<option value="0">No</option>
				}
				@endif
				@if($items[0]['patient_painstorypublic'] == 1)
				{
					<option value="1" selected="selected">Yes</option>
				}
				@else
				{
					<option value="1">Yes</option>
				}
				@endif
			</select>
		</div>
		<div class="form-group">
			<label for="story">Pain Story</label>
			<textarea name="story" id="story" class="form-control" rows="10">{{$items[0]['story']}}</textarea>
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-success">Update</button>
		</div>
	</form>
@stop
