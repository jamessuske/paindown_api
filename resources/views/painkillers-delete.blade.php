@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Pain Killers</h1>
@stop

@section('content')
    <div class="form-group">
		<label>Pain Killer Name: </label>
		<span>{{$data->painkiller_name}}</span>
	</div>
	<form method="POST" action="/paindown_api/public/admin/painkillers-delete/{{ $data->pkpainkiller }}">
		<div class="form-group">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<button type="submit" class="btn btn-success">Delete</button>
		</div>
	</form>
@stop
