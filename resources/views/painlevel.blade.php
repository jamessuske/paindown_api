@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 style="float: left;">Pain Levels</h1>
	<a href="painlevel-add" class="btn btn-success" style="float: right;">Add New</a>
@stop

@section('content')
    <table class="table table-dark" style="clear: both;">
		<tr>
			<th>Name</th>
			<th></th>
			<th></th>
		</tr>
	@foreach ($data as $item)
		<tr><td>{{ $item->name }}</td><td><a href="painlevel-edit/{{$item->id}}" class="btn btn-default">Edit</a></td><td><a href="painlevel-delete/{{$item->id}}" class="btn btn-default">Delete</a></td></tr>
	@endforeach
	</table>
@stop
