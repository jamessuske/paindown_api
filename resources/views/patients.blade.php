@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 style="float: left;">Patients</h1>
	<a href="patients-add" class="btn btn-success" style="float: right;">Add New</a>
@stop

@section('content')
	<table class="table table-dark" style="clear: both;">
		<tr>
			<th>Username</th>
			<th>Email</th>
			<th>Image</th>
			<th>Pain Years</th>
			<th>Pain Level</th>
			<th>Pain Type</th>
			<th>Pain Killers</th>
			<th></th>
			<th></th>
		</tr>
	@foreach ($items as $item)
		<tr>
			<td>{{ $item['username'] }}</td>
			<td>{{ $item['email'] }}</td>
			<td><img src="{{ $item['image'] }}" height="50" /></td>
			<td>{{ $item['pain_years'] }}</td>
			<td>{{ $item['pain_level'] }}</td>
			<td>{{ $item['pain_type'] }}</td>
			<td>{{ $item['usage_painkiller'] }}</td>
			<td><a href="patients-edit/{{$item['pkpatient']}}" class="btn btn-default">Edit</a></td><td><a href="patients-delete/{{$item['pkpatient']}}" class="btn btn-default">Delete</a></td>
		</tr>
	@endforeach
	</table>
@stop
