@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Professionals</h1>
@stop

@section('content')
	<form method="POST" action="/paindown_api/public/admin/professionals-edit/{{ $items[0]['pkprofessional'] }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="form-group">
			<label for="professional_username">Username</label>
			<input type="text" name="professional_username" id="professional_username" class="form-control" value="{{$items[0]['username']}}" />
		</div>
		<div class="form-group">
			<label for="name">Name</label>
			<input type="text" name="name" id="name" class="form-control" value="{{$items[0]['name']}}" />
		</div>
		<div class="form-group">
			<label for="image">Image</label>
			<input type="file" name="image" id="image" class="form-control" />
			<input type="hidden" name="image" id="image" class="form-control" value="{{$items[0]['image']}}" />
		</div>
		<div class="form-group">
                        <label for="role">Role</label>
                        <select name="role" id="role" class="form-control">
                                <option id="0">-- Select Role --</option>
                                @foreach ($roles as $r)
                                        @if($r->id == $items[0]['pain_level'])
                                                <option id="{{$r->id}}" selected>{{$r->text}}</option>
                                        @else
                                                <option id="{{$r->id}}">{{$r->text}}</option>
                                        @endif
                                @endforeach
                        </select>
                </div>
		<div class="form-group">
			<label for="profession_name">Profession</label>
			<select name="profession_name" id="profession_name" class="form-control">
				<option id="0">-- Select Profession --</option>
				@foreach ($profession as $p)
					@if($p->id == $items[0]['profession_name'])
						<option id="{{$p->id}}" selected>{{$p->text}}</option>
					@else
						<option id="{{$p->id}}">{{$p->text}}</option>
					@endif
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="speciality">Speciality</label>
			<select name="speciality" id="speciality" class="form-control" multiple>
				<option id="0">-- Select Specialities --</option>
				@foreach ($speciality as $s)
					@if(in_array($s->id, $items[0]['speciality']))
						<option id="{{$s->id}}" selected>{{$s->text}}</option>
					@else
						<option id="{{$s->id}}">{{$s->text}}</option>
					@endif
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="about">About</label>
			<textarea name="about" id="about" class="form-control" rows="10">{{$items[0]['about']}}</textarea>
		</div>
		<div class="form-group">
			<label for="rating">Rating</label>
			<input type="number" step="0.5" max="5" min="0" name="rating" id="rating" class="form-control" value="{{$items[0]['rating']}}" />
		</div>
		<div class="form-group">
			<label for="latitude">Latitude</label>
			<input type="text" name="latitude" id="latitude" class="form-control" value="{{ $items[0]['position']['lat'] }}" />
		</div>
		<div class="form-group">
			<label for="longitude">Longitude</label>
			<input type="text" name="longitude" id="longitude" class="form-control" value="{{ $items[0]['position']['long'] }}" />
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-success">Update</button>
		</div>
	</form>
@stop
