@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Pain Types</h1>
@stop

@section('content')
    <div class="form-group">
		<label>Pain Type Name: </label>
		<span>{{$data->paintype_name}}</span>
	</div>
	<form method="POST" action="/paindown_api/public/admin/paintype-delete/{{ $data->pkpaintype }}">
		<div class="form-group">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<button type="submit" class="btn btn-success">Delete</button>
		</div>
	</form>
@stop
