@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Specialities</h1>
@stop

@section('content')
	<form method="POST" action="/paindown_api/public/admin/specialities-add">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="form-group">
			<label for="specialty_name">Specialities Name</label>
			<input type="text" name="specialty_name" id="specialty_name" class="form-control" />
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-success">Add</button>
		</div>
	</form>
@stop
