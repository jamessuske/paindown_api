@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Professions</h1>
@stop

@section('content')
    <div class="form-group">
		<label>Profession Name: </label>
		<span>{{$data->profession_name}}</span>
	</div>
	<form method="POST" action="/paindown_api/public/admin/professions-delete/{{ $data->pkprofession }}">
		<div class="form-group">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<button type="submit" class="btn btn-success">Delete</button>
		</div>
	</form>
@stop
